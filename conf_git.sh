#!/bin/bash

echo "Usuario Actual es:"
git config -l | grep user.name
echo "Correo Actual es:"
git config -l | grep user.email
echo "Deseas cambiar de usuario? (s/n)"
read respuesta
if [ $respuesta = "s" ]
then
    echo "Ingresa el nuevo usuario"
    read nuevo_usuario
    git config --global user.name $nuevo_usuario
    echo "Ingresa el nuevo correo"
    read nuevo_correo
    git config --global user.email $nuevo_correo
    echo "Usuario y correo cambiados"
else
    echo "No se cambiaron los datos"
fi

export interface Role {
    id: number;
    name: string;
    description: string;
    permissions: number[];
}

export const roleInitializer: Role = {
    id: 0,
    name: '',
    description: '',
    permissions: [],
}
export interface CommentRaw {
    member: number;
    content: string;
}
export interface MemberRaw {
    user: string;
    project: number | string;
    projectRole: number;
    is_active: boolean;
    availability: number;
}

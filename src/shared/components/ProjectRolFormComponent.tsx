import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import { makeStyles, Theme } from '@material-ui/core';
import { useState, useContext } from 'react';
import { ProjectRolContext } from '../../data/providers/rolProyecto/rolProyecto-context';
import { ProjectRole, roleInitializer } from '../interfaces/projectRole.interface';
import PermissionsChecker from './PermissionsChecker';
import { ProjectPermissionsCode } from '../../data/constants/permisos';
import { ErrorContext } from '../../data/providers/error/error-context';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';

const useStyles = makeStyles((theme: Theme) => ({
    inputsContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    inputs: {
        marginBottom: 30,
    },
    inputsChildren: {
        marginBottom: 30,
        width: '48%',
    },
    button: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
    buttonDelete: {
        backgroundColor: "#FF0000",
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
}));

type rolProps =
    {
        open: boolean;
        cerrarModal: () => void;
        savedRol?: ProjectRole;
        handleSave?: (rol: ProjectRole) => Promise<void>;
        handleUpdate?: (rol: ProjectRole) => Promise<void>;
        handleDelete?: (rol: ProjectRole) => Promise<void>;
    }

export default function RolFormComponent(props: rolProps) {

    const [formData, setFormData] = useState<ProjectRole>(props.savedRol || roleInitializer);

    const classes = useStyles();

    const { state: { currentProject } } = useContext(ProyectoContext);

    const { state } = useContext(ProjectRolContext);

    const { dialog, setDialog, handleClose } = useContext(ErrorContext);

    useEffect(() => props.savedRol && setFormData(props.savedRol as ProjectRole), [props.savedRol]);

    const submit = async () => {
        if (!formData.permissions.length) {
            setDialog({
                title: 'Atención ⚠️',
                body: "No puede existir un Rol sin Permisos",
                buttonLabel: 'Aceptar',
                open: true
            });
            return
        }
        if (props.handleSave) {
            await props.handleSave(formData);
        }
        if (props.handleUpdate) {
            await props.handleUpdate(formData);
        }
        setFormData(roleInitializer);
        props.cerrarModal();
    }


    return (
        <div>
            <Dialog open={props.open} onClose={props.cerrarModal} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{!props.savedRol ? "Nuevo Rol de Proyecto" : "Editar Rol de Proyecto"}</DialogTitle>
                <DialogContent>
                    <TextField
                        value={formData.name}
                        onChange={(event) => setFormData({ ...formData, name: event.target.value })}
                        required
                        autoFocus={!props.savedRol}
                        margin="dense"
                        disabled={currentProject.status === 'FINALIZADO'}
                        id="rolName"
                        label="Nombre"
                        placeholder="Nombre del Rol"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                    />
                    <TextField
                        value={formData.description}
                        onChange={(event) => setFormData({ ...formData, description: event.target.value })}
                        margin="dense"
                        id="rolDescription"
                        label="Descripción"
                        placeholder="Descripción del Rol"
                        multiline
                        rows={4}
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        disabled={currentProject.status === 'FINALIZADO'}

                    />
                    {!state.loading &&
                        <List subheader={<ListSubheader disableSticky={true}>Permisos</ListSubheader>} /*className={classes.root}*/>
                            {state.permisos.map(item => (
                                <ListItem key={`permiso-${item.id}`}>
                                    <ListItemText id={`permiso-${item.id}`} primary={`${item.name}`} />
                                    <ListItemSecondaryAction>
                                        <Switch
                                            color="primary"
                                            edge="end"
                                            disabled={currentProject.status === 'FINALIZADO'}
                                            onChange={() => {
                                                let itemIndex = formData.permissions.indexOf(item.id);
                                                let permisosActuales = [...formData.permissions];
                                                if (itemIndex === -1)
                                                    permisosActuales.push(item.id)
                                                else
                                                    permisosActuales.splice(itemIndex, 1)
                                                setFormData({ ...formData, permissions: permisosActuales })
                                            }}
                                            checked={formData.permissions.indexOf(item.id) !== -1}
                                            inputProps={{ 'aria-labelledby': `permiso-${item.id}` }}
                                        />
                                    </ListItemSecondaryAction>
                                </ListItem>))
                            }
                        </List>}
                </DialogContent>
                <DialogActions>
                    <PermissionsChecker
                        tipo="Project"
                        permissions={!props.savedRol ? [] : [ProjectPermissionsCode.ELIMINAR_ROL_DE_PROYECTO,]}
                    >
                        <Button onClick={async () => {
                            if (!props.savedRol) {
                                props.cerrarModal();
                            } else if (props.handleDelete) {
                                await props.handleDelete(props.savedRol);
                                props.cerrarModal();
                            }
                        }} className={!props.savedRol ? classes.button : classes.buttonDelete}>
                            {!props.savedRol ? "Cancelar" : "Eliminar"}
                        </Button>
                    </PermissionsChecker>
                    {currentProject.status !== "FINALIZADO" && <PermissionsChecker
                        tipo="Project"
                        permissions={!props.savedRol ? [ProjectPermissionsCode.CREAR_ROL_DE_PROYECTO,] : [ProjectPermissionsCode.MODIFICAR_ROL_DE_PROYECTO,]}
                    >
                        <Button onClick={submit} className={classes.button}>
                            {!props.savedRol ? "Crear" : "Guardar"}
                        </Button>
                    </PermissionsChecker>
                    }
                </DialogActions>
            </Dialog>
        </div >
    );
}
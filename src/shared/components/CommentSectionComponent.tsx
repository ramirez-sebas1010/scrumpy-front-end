import React, { FC, useContext, useState } from 'react'
import { Box, makeStyles, TextField, Theme, Typography } from '@material-ui/core'
import { CommentRaw } from '../interfaces/comment-raw.interface';
import { CommentContext } from '../../data/providers/comment/comment-context';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import CommentCardComponent from './CommentCardComponent';
import { postComment, putComment } from '../../data/providers/comment/comment-action-creators';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import { Comment } from '../interfaces/comment.interface';

const useStyles = makeStyles((theme: Theme) => ({

}));

type CommentProps = {
    readOnly?: boolean;
}

const CommentSectionComponent: FC<CommentProps> = ({ readOnly = false }) => {
    const { state: { comments, loading }, dispatch } = useContext(CommentContext);
    const { state: { currentMember } } = useContext(MiembroContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);

    const [comment, setComment] = useState<CommentRaw>({
		member: currentMember.id,
		content: '',
	});

    const submitComment = async () => {
		await postComment(dispatch, currentProject.id, currentUS.id, comment);
		setComment({ ...comment, content: '' })
	}

    const updateComment = async (comment: Partial<Comment>) => {
        await putComment(dispatch, currentProject.id, currentUS.id, comment);
    }

    return (
        <>
            <Box
                style={{
                    height: '75%',
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                }}
            >
                {
                    !loading && comments.map(comment => (
                        <CommentCardComponent 
                        key={comment.id} 
                        comment={comment} 
                        handleUpdate={updateComment} 
                        readOnly={readOnly} />
                    ))
                }
            </Box>


            <TextField
                style={{
                    backgroundColor: '#fff',
                    width: '100%',
                    minHeight: '14%'
                }}
                disabled={ readOnly }
                value={comment.content}
                onChange={event => setComment({ ...comment, content: event.target.value })}
                margin="dense"
                placeholder="Agregue un comentario"
                multiline
                rows={4}
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                onKeyPress={event => event.key === 'Enter' && submitComment()}
            />
        </>
    )
}

export default CommentSectionComponent

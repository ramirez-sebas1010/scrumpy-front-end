import { Container, Typography, CardContent, CardActions, Button, Card, Theme, makeStyles, createStyles } from '@material-ui/core';
import { AddCircleOutlined } from '@material-ui/icons';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


import { FC, useContext, useEffect, useState } from 'react'
import { DragDropContext, Droppable, DropResult, ResponderProvided, Draggable } from 'react-beautiful-dnd';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import UserStoryCard from './UserStoryCardComponent';
import { postUserStory, setCurrentUS, updateUserStory } from '../../data/providers/userstory/user-story-action-creators';
import { UserStory, userStoryInitializer } from '../interfaces/user_story.interface';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import userStoryAPI from '../../api/userStoryScrumpyAPI';
import { SprintContext } from '../../data/providers/sprint/sprint-context';
import { ErrorContext } from '../../data/providers/error/error-context';
import DialogComponent from './DialogComponent';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },

        'card-': {
            display: "flex",
            justifyContent: 'space-between',
            alignItems: 'start',
            cursor: 'pointer',
        },
        'add-us': {
            display: "flex",
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        'list-card': {
            height: '400px',
        }

    }),
);

type TableProps = {
    productBacklog: UserStory[];
    sprintBacklog: UserStory[];
    handleChange: (pb: UserStory[], sb: UserStory[]) => void;
}

export const DragNDropTable : FC<TableProps> = ({ productBacklog, sprintBacklog, handleChange }) => {
    const classes = useStyles();
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { userStories }, dispatch } = useContext(UserStoryContext);
    const { state: { currentSprint } } = useContext(SprintContext);
    const { dialog, setDialog, handleClose:closeError } = useContext(ErrorContext);

    const [formData, setFormData] = useState(userStoryInitializer);

    const [open, setOpen] = useState(false);
    const [openUSCard, setOpenUSCard] = useState(false);
    const [savedUS, setSavedUS] = useState<UserStory>(userStoryInitializer);

    

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleName = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setFormData((prev_state) => {
            return {
                ...prev_state,
                name: event.target.value,
                project: currentProject.id,

            }

        });

    }
    const handleDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();

        setFormData((prev_state: any) => {
            return {
                ...prev_state,
                description: event.target.value,
                project: currentProject.id,
            }
        });


    }

    const handlePriority = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setFormData((prev_state: any) => {
            return {
                ...prev_state,
                priority: event.target.value,
                project: currentProject.id,
            }
        });

    }


    const crearUS = async () => {
        //Create User Story
        console.log(formData);
        console.log("Crear US");
        try {
            await postUserStory(dispatch, formData);

            console.log('creado');
        } catch (error: any) {
            alert(error.message)
        }


        setOpen(false);
    }

    // sprint null -> pb
    //no sprint -> sb
    //Funcion donde realiza el entre columnas, etc
    function handleOnDragEnd(result: DropResult, provided: ResponderProvided) {
        const { draggableId, destination, source, combine } = result;
        console.log(result);

        //Verifican si esta en el mismo tablero
        if (destination?.droppableId === source.droppableId) {
            console.log('Moviendo en la Misma Lista')
            reorderCurrentList();
            return
        }

        if (source.droppableId === 'product-log' && destination?.droppableId === 'sprint-log') {
            console.log('Moviendo de PB a SB')
            //Si se mueve de product backlog a sprint backlog
            if(currentSprint.status === 'EN_PLANIFICACION'){
                const newSprintBacklog = [...sprintBacklog];
                const newProductBacklog = [...productBacklog];
                const [removed] = newProductBacklog.splice(source.index, 1);
                newSprintBacklog.splice(destination.index, 0, removed);
                userStoryAPI.moverUserStoryASB(currentProject.id, currentSprint.id, removed.id)
                .then((updated) => {
                    console.log(updated)
                    dispatch(updateUserStory(updated));
                })
                handleChange(newProductBacklog, newSprintBacklog);
            } else {
                setDialog({
                    title: 'Atención ⚠️',
                    body: 'No puedes agregar user stories a un sprint iniciado!',
                    buttonLabel: 'Aceptar',
                    open: true
                });
            }
            return;
        }

        if (source.droppableId === 'sprint-log' && destination?.droppableId === 'product-log') {
            console.log('Moviendo de SB a PB');
            //Si se mueve de sprint backlog a product backlog
            const newSprintBacklog = [...sprintBacklog];
            const newProductBacklog = [...productBacklog];
            const [removed] = newSprintBacklog.splice(source.index, 1);
            newProductBacklog.splice(destination.index, 0, removed);
            userStoryAPI.moverUserStoryAPB(currentProject.id, currentSprint.id, removed.id)
            .then((updated) => {
                console.log(updated);
                dispatch(updateUserStory(updated));
            })
            handleChange(newProductBacklog, newSprintBacklog);
            return;
        }

        function reorderCurrentList() {
            console.log(`Moviendo en el Tablero ${source.droppableId}`);
            const isProductBacklog = productBacklog.filter(item => String(item.id) === draggableId)[0];
            const items = Array.from(isProductBacklog ? productBacklog : sprintBacklog);
            const [reorderedItem] = items.splice(result.source.index, 1);

            if (result.destination) {
                items.splice(result.destination.index, 0, reorderedItem);
                isProductBacklog ? handleChange(items, sprintBacklog) : handleChange(productBacklog, items);
            }
        }

    }

    return (
        <>
            <Container style={{ width: '100%', display: 'flex' }}>
                <DragDropContext onDragEnd={handleOnDragEnd}>
                    <Container className={classes['list-card']}>
                        <Container className={classes['add-us']}>
                            <Typography variant="h6">Product Backlog</Typography>
                            <Button onClick={handleClickOpen} > <AddCircleOutlined /></Button>
                        </Container>

                        <Droppable droppableId="product-log">
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    style={{
                                        padding: 4,
                                        backgroundColor: snapshot.isDraggingOver ? '#f5f5f5' : '#fff',
                                        width: '100%',
                                        height: '100%',
                                        overflow: 'auto',

                                    }}
                                >
                                    {productBacklog.map((item, index) => (
                                        <Draggable key={item.id} draggableId={String(item.id)} index={index}>
                                            {(provided) => (
                                                <Card onClick={() => {setSavedUS(item); setOpenUSCard(true); dispatch(setCurrentUS(item));}} className={classes['card-']} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                    <CardContent>
                                                        <Typography>{item.name}</Typography>
                                                        <Typography>#{item.id}</Typography>
                                                    </CardContent>
                                                    <CardActions>
                                                        <Button size="small"><MoreVertIcon /></Button>
                                                    </CardActions>
                                                </Card>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>

                    </Container>
                    <Container className={classes['list-card']}>
                        <Container className={classes['add-us']}>
                            <Typography variant="h6">Sprint Backlog</Typography>

                        </Container>
                        <Droppable droppableId="sprint-log">
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    style={{
                                        backgroundColor: snapshot.isDraggingOver ? '#f5f5f5' : '#fff',
                                        padding: 4,
                                        width: '100%',
                                        height: '100%',
                                        overflow: 'auto',
                                    }}
                                >
                                    {sprintBacklog.map((item, index) => (
                                        <Draggable key={item.id} draggableId={String(item.id)} index={index}>
                                            {(provided) => (
                                                <Card onClick={() => {setSavedUS(item); setOpenUSCard(true); dispatch(setCurrentUS(item));}} className={classes['card-']} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                    <CardContent>
                                                        <Typography>{item.name}</Typography>
                                                        <Typography>#{item.id}</Typography>
                                                    </CardContent>
                                                    <CardActions>
                                                        <Button size="small"><MoreVertIcon /></Button>
                                                    </CardActions>
                                                </Card>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </Container>
                </DragDropContext>
            </Container>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Crear User Story</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Crear User Story al Product Backlog
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        variant="outlined"
                        id="nombre"
                        onChange={handleName}
                        label="Nombre"
                        type="text"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        variant="outlined"
                        id="descripcion"
                        multiline
                        rows={4}
                        onChange={handleDescription}
                        label="Descripcion"
                        type="text"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        variant="outlined"
                        id="prioridad"
                        onChange={handlePriority}
                        label="Prioridad"
                        type="number"
                        InputProps={{ inputProps: { min: 0, max: 10 } }}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={crearUS} color="primary">
                        Crear
                    </Button>
                </DialogActions>
            </Dialog>
            <UserStoryCard isOpen={openUSCard} handleClose={() => setOpenUSCard(false)} savedUserStory={savedUS} />
            <DialogComponent dialog={dialog} handleClose={closeError} />
        </>
    )
}

export default DragNDropTable;

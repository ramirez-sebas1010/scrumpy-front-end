import React, { useContext, FC } from 'react';
import { RolContext } from '../../data/providers/rol/rol-context';
import { ProjectRolContext } from '../../data/providers/rolProyecto/rolProyecto-context';

type PropsType = {
    permissions: number[];
    tipo?: 'System' | 'Project';
}

const PermissionsChecker: FC<PropsType> = ({ permissions, children, tipo="System" }) => {

    const { hasPermission } = useContext(RolContext);
    const { hasPermission: hasProjectPermission } = useContext(ProjectRolContext);

    if (permissions.length === 0 || permissions.some(
        permission => tipo === 'System' ? hasPermission(permission) : hasProjectPermission(permission)
    ))
        return <> { children} </>

    return <></>;
}

export default PermissionsChecker;
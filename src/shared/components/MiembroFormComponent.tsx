import React, { useContext } from 'react';
import { useFormik, getIn } from 'formik';
import * as yup from 'yup';
import { Box, makeStyles, Switch, Theme, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { UsuarioContext } from '../../data/providers/usuario/usuario-context';
import { Member, MemberInitializer } from '../interfaces/miembro.interface';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { ProjectRole } from '../interfaces/projectRole.interface';
import PermissionsChecker from './PermissionsChecker';
import { ProjectPermissionsCode } from '../../data/constants/permisos';
import { SprintContext } from '../../data/providers/sprint/sprint-context';
import { ErrorContext } from '../../data/providers/error/error-context';
import DialogComponent from './DialogComponent';
import { AuthContext } from '../../data/providers/auth/auth-context';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import { Usuario } from '../interfaces/usuario.interface';

const useStyles = makeStyles((theme: Theme) => ({
    inputsContainer: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    inputs: {
        marginBottom: 30,
    },
    inputsChildren: {
        marginBottom: 30,
        width: '48%',
    },
    button: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
    buttonDelete: {
        backgroundColor: "#FF0000",
        marginRight: 15,
        marginBottom: 15,
        marginTop: 0,
        color: '#FFFFFF',
        padding: 10,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));



type rolProps =
    {
        open: boolean;
        rolList: ProjectRole[];
        miembro?: Member;
        save: (miembro: Member) => void;
        delete?: (miembro: Member) => void;
        cerrarModal: () => void;
    }

const miembroInicial: Member = MemberInitializer;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


export default function MiembroFormComponent(props: rolProps) {

    const { dialog, setDialog, handleClose: handleCloseDialogError } = useContext(ErrorContext);
    const { state: { auth: { usuario: currentUser } } } = useContext(AuthContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentSprint } } = useContext(SprintContext);
    const { state: { usuarios } } = useContext(UsuarioContext);
    const { state: { miembros } } = useContext(MiembroContext);

    const isCreateMode = props.miembro === undefined || props.miembro === null;

    const isScrumMasterUser = !isCreateMode && ((currentProject.scrum_master as Usuario).id === props.miembro!.user.id);

    let rolList: ProjectRole[] = [];

    if (isScrumMasterUser) {
        rolList = props.rolList;
    } else {
        const scrumMember = miembros.find(member => member.user.id === (currentProject.scrum_master as Usuario).id);
        rolList = props.rolList.filter(rol => rol.id != scrumMember!.id);
    }

    const classes = useStyles();

    const validationSchema = yup.object().shape(
        {
            user: yup.object().shape({
                email: yup.string()
                    .email("Ingrese un correo valido")
                    .test("correo-existente", "Este correo no existe en la lista de usuarios", async (value, testContext) => {
                        await sleep(500);
                        let isHave = usuarios.find(user => user.email === value);
                        return isHave === undefined ? false : true;
                    })
                    .test("correo-duplicado", "Este correo ya se encuentra agregado", (value, testContext) => {
                        if (!isCreateMode) {
                            return true
                        } else {
                            let isHave = miembros.find(member => member.user.email === value);
                            return isHave === undefined ? true : false;
                        }
                    })
                    .required("El correo es requerido"),
            }),
            projectRole: yup.object().shape({
                name: yup.string()
                    .min(1, "Requerido")
                    .required("Requerido"),
            }),
            is_active: yup.boolean(),
            availability: yup.number()
                .typeError("Debe ser un numero")
                .min(0, "No puede ser un valor negativo")
                .required("Requerido"),
        }
    );

    const getRolById = (prop: number) => {
        const result = rolList.find(({ id }) => id === prop);
        if (result === undefined) {
            return miembroInicial.projectRole;
        } else {
            return result;
        }
    }

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: props.miembro === undefined || props.miembro === null ? miembroInicial : props.miembro,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            props.save(values);
            handleClose();
        },
    });

    const handleClose = () => {
        props.cerrarModal();
        sleep(100).then(() => {
            formik.resetForm();
        })
    };

    const handleDelete = () => {
        props.cerrarModal();
        let values = formik.values;
        sleep(100).then(() => {
            formik.resetForm();
        })

        if (props.delete !== undefined) {
            props.delete(values);
        }
    }

    return (
        <div>
            <Dialog open={props.open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">{!props.miembro ? "Nuevo miembro" : "Editar miembro"}</DialogTitle>
                    <DialogContent >
                        <TextField
                            name="user.email"
                            label="Email"
                            placeholder="Correo del miembro"
                            disabled={!isCreateMode || currentSprint.status === 'FINALIZADO'}
                            autoFocus={!props.save}
                            fullWidth
                            margin="dense"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}

                            value={formik.values.user.email}
                            onChange={formik.handleChange}
                            error={Boolean(
                                getIn(formik.touched, 'user.email') &&
                                getIn(formik.errors, 'user.email')
                            )}
                            helperText={
                                getIn(formik.touched, 'user.email') &&
                                getIn(formik.errors, 'user.email')
                            }
                        />
                        <Box mt={2}>
                            <Grid container direction="row" spacing={2} justifyContent="flex-start" alignItems="center">
                                <Grid item>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel
                                            id="select-label"
                                            error={formik.touched.projectRole && Boolean(formik.errors.projectRole)}
                                        >
                                            Rol
                                        </InputLabel>
                                        <Select
                                            labelId="select-label"
                                            name="projectRol"
                                            disabled={isScrumMasterUser || currentSprint.status === 'FINALIZADO'}
                                            value={formik.values.projectRole!.id}
                                            onChange={(event) => {
                                                formik.setValues({
                                                    ...formik.values,
                                                    projectRole: getRolById(event.target.value as number),
                                                })
                                            }}
                                            error={formik.touched.projectRole && Boolean(formik.errors.projectRole)}
                                        >
                                            {
                                                rolList.map((rol) => <MenuItem key={rol.id} value={rol.id}>{rol.name}</MenuItem>)
                                            }
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item >
                                    <TextField
                                        name="availability"
                                        label="Disponibilidad"
                                        placeholder="Semanal en horas"
                                        fullWidth
                                        margin="dense"
                                        variant="outlined"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        disabled={currentProject.status === 'FINALIZADO'}
                                        value={formik.values.availability}
                                        onChange={formik.handleChange}
                                        error={Boolean(formik.touched.availability && formik.errors.availability)}
                                        helperText={formik.touched.availability && formik.errors.availability}
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                        <Box mt={2}>
                            <Grid container direction="row" spacing={2} justifyContent="flex-end" alignItems="center">
                                <Grid item>
                                    <Typography style={{ textAlign: 'left' }} variant="subtitle1" >Activo</Typography>
                                </Grid>
                                <Grid item>
                                    <Switch
                                        name="is_active"
                                        color="primary"
                                        edge="end"
                                        disabled={isScrumMasterUser || currentSprint.status === 'FINALIZADO'}
                                        onChange={formik.handleChange}
                                        checked={formik.values.is_active}
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        {currentProject.status !== 'FINALIZADO' && <PermissionsChecker
                            tipo="Project"
                            permissions={!props.miembro ? [] : [ProjectPermissionsCode.ELIMINAR_MIEMBRO,]}
                        >
                            <Button onClick={() => {
                                if (!props.miembro) {
                                    handleClose();
                                } else {
                                    if ((currentProject.scrum_master as Usuario).id === props.miembro.user.id) {
                                        setDialog({
                                            title: 'Atención ⚠️',
                                            body: "El Scrum Master no puede ser eliminado",
                                            buttonLabel: 'Aceptar',
                                            open: true
                                        });
                                    } else if (currentUser.id === props.miembro.user.id) {
                                        setDialog({
                                            title: 'Atención ⚠️',
                                            body: "Usted no puede eliminarse a si mismo",
                                            buttonLabel: 'Aceptar',
                                            open: true
                                        });
                                    }
                                    else if (props.miembro.is_active) {
                                        setDialog({
                                            title: 'Atención ⚠️',
                                            body: "Un miembro activo no puede eliminarse",
                                            buttonLabel: 'Aceptar',
                                            open: true
                                        });
                                    } else if (currentSprint.status !== "FINALIZADO" && props.miembro.is_active) {
                                        setDialog({
                                            title: 'Atención ⚠️',
                                            body: "No se puede eliminar debido a que el sprint aún no ha terminado",
                                            buttonLabel: 'Aceptar',
                                            open: true
                                        });
                                    } else {
                                        handleDelete();
                                    }
                                }
                            }} className={!props.miembro ? classes.button : classes.buttonDelete}>
                                {!props.miembro ? "Cancelar" : "Eliminar"}
                            </Button>
                        </PermissionsChecker>
                        }
                        {currentProject.status !== 'FINALIZADO' && <PermissionsChecker
                            tipo="Project"
                            permissions={!props.miembro ? [ProjectPermissionsCode.AGREGAR_MIEMBRO] : [ProjectPermissionsCode.MODIFICAR_MIEMBRO,]}
                        >
                            <Button className={classes.button} type="submit">
                                {!props.miembro ? "Agregar" : "Actualizar"}
                            </Button>
                        </PermissionsChecker>
                        }
                    </DialogActions>
                </form>
            </Dialog>
            <DialogComponent dialog={dialog} handleClose={handleCloseDialogError} />
        </div >
    );
}
import React, { useContext, useState, FC, useEffect } from 'react'
import { Modal, Select, MenuItem, Container, Typography, Divider, TextField, Button, createStyles, makeStyles, Theme, Box, Grid } from '@material-ui/core'
import DayJsUtils from '@date-io/dayjs';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { UsuarioContext } from '../../data/providers/usuario/usuario-context';
import { Proyecto } from '../interfaces/proyecto.interface';
import { Usuario } from '../interfaces/usuario.interface';
import { proyectoInitializer } from '../interfaces/proyecto.interface';
import { AuthContext } from '../../data/providers/auth/auth-context';
import dayjs from 'dayjs';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        myProyects: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly'
        },
        showingProjectCards: {
            display: 'flex',
            flexWrap: 'wrap'
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '20px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
        },
    }),
);

type modalProps = {
    open: boolean;
    handleOpen: () => void;
    handleClose: () => void;
    handleSave: (proyecto: Proyecto) => Promise<void>;
}

const CreateProjectFormComponent: FC<modalProps> = ({ open, handleClose, handleSave }) => {
    const classes = useStyles();

    const [formData, setFormData] = useState(proyectoInitializer);

    const now = dayjs();

    const [selectedDate, handleDateChange] = useState(now);


    const { state: { usuarios } } = useContext(UsuarioContext);
    const { state: { auth } } = useContext(AuthContext);

    const users: Usuario[] = usuarios;
    const currentUser: Usuario = auth.usuario;

    useEffect(() => {
        setFormData({ ...formData, scrum_master: currentUser.email });
    }, []);

    const handleTitulo = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        let a = event.target.value;
        setFormData((prev) => (
            {
                ...prev,
                name: a
            }
        ));
    }

    const handleDescripcion = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        let a = event.target.value;
        setFormData((prev) => (
            {
                ...prev,
                description: a
            }
        ));
    }


    const crearProyecto = async () => {
        try {
            let formDataPost: Proyecto = {
                ...formData,
                startDay: now.format("YYYY-MM-DD"),
                endDay: selectedDate.format("YYYY-MM-DD"),
            }
            await handleSave(formDataPost);
        } catch (error) {
            console.log(error);
        }
    }

    const menus = () => {
        return users.map((element) => {
            return (
                <MenuItem key={element.id} value={element.email}>
                    <em>{element.first_name}</em>
                </MenuItem>
            );
        });
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <Container style={{
                height: "70%",
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                width: "50%",
                backgroundColor: "white",
                marginTop: '60px'
            }}>
                <div style={{
                    width: '100%',
                    height: '100%',
                    padding: '50px'
                }}>
                    <Typography style={{ textAlign: 'center' }} variant="h3" >Nuevo Proyecto</Typography>
                    <br />
                    <Divider></Divider>
                    <br />
                    <form style={{ display: 'flex', flexDirection: 'column' }}>
                        <TextField id="titulo-proyecto" onChange={handleTitulo} label="Titulo del Proyecto" variant="outlined" />
                        <br />
                        <TextField id="descripcion-proyecto" onChange={handleDescripcion} aria-colcount={4} multiline maxRows={4} label="Descripcion del Proyecto" variant="outlined" />
                        <br />
                        <Box>
                            <MuiPickersUtilsProvider utils={DayJsUtils} >
                                <DatePicker
                                    label="Fecha de finalización"
                                    value={selectedDate}
                                    format="DD/MM/YYYY"
                                    minDate={now}
                                    onChange={(value) => {
                                        let date = value!;
                                        handleDateChange(date);
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Box>
                        <Box mt={2.5}>
                            <TextField
                                id="outlined-read-only-input"
                                disabled={true}
                                label="Scrum Master"
                                defaultValue={currentUser.first_name + " " + currentUser.last_name}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        </Box>
                        <Box className={classes.buttonCreate}>
                            <Button onClick={crearProyecto} variant="contained" color="primary" component="span">
                                Crear Proyecto
                            </Button>
                        </Box>
                    </form>
                </div>
            </Container>
        </Modal>
    )


}

export default CreateProjectFormComponent;
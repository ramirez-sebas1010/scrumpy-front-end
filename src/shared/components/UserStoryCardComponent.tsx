import { FC, useCallback, useContext, useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Avatar, Button, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Fab, makeStyles, MenuItem, Modal, Select, TextField, Theme } from '@material-ui/core';
import { UserStory, userStoryInitializer } from '../interfaces/user_story.interface';
import { Add as AddIcon, Edit } from '@material-ui/icons';
import { Member } from '../interfaces/miembro.interface';
import MemberPickerComponent from './MemberPickerComponent';
import { MiembroContext } from '../../data/providers/miembro/miembro-context';
import { UserStoryContext } from '../../data/providers/userstory/user-story-context';
import { putUserStory } from '../../data/providers/userstory/user-story-action-creators';
import CommentSectionComponent from './CommentSectionComponent';
import { ProyectoContext } from '../../data/providers/proyecto/proyecto-context';
import LoaderComponent from './LoaderComponent';
import PermissionsChecker from './PermissionsChecker';
import { ProjectPermissionsCode } from '../../data/constants/permisos';
import { ProjectRolContext } from '../../data/providers/rolProyecto/rolProyecto-context';
import ActivitySectionComponent from './ActivitySectionComponent';
import EventUSSectionComponent from './EventSectionComponent';
import { ActivityContext } from '../../data/providers/activity/activity-context';

const useStyles = makeStyles((theme: Theme) => ({
	modal: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
	},
	container: {
		backgroundColor: '#fff',
		borderRadius: 15,
		padding: 0,
	},
	content: {
		width: '100%',
		height: '70vh',
		minHeight: 650,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	main: {
		width: '70%',
		padding: 25,
	},
	title: {
		fontSize: 16,
		color: '#595151',
		marginBottom: 10,
	},
	storyName: {
		fontSize: 27,
		color: '#070000',
		marginRight: 20,
	},
	storyDescription: {
		color: '#070000',
		marginBottom: 20,
	},
}));

type UserStoryProps = {
	isOpen: boolean;
	handleClose: () => void,
	savedUserStory?: UserStory;
	handleSave?: (usuario: UserStory) => Promise<void>,
	handleUpdate?: (usuario: UserStory) => Promise<void>,
	readOnly?: boolean;
}

const options = [
	{id: 1, label: 'Comentarios' },
	{id: 2, label: 'Actividades' },
	{id: 3, label: 'Historial' },
];

const UserStoryCardComponent: FC<UserStoryProps> = (props) => {
	const { isOpen, handleClose, savedUserStory, readOnly = false } = props;
	const [formData, setFormData] = useState<UserStory>(savedUserStory || userStoryInitializer);
	const [openMiembros, setOpenMiembros] = useState<boolean>(false);
	const [selectedMember, setSelectedMember] = useState<Member>();
	const { state: { currentProject } } = useContext(ProyectoContext);
	const { state: { miembros } } = useContext(MiembroContext);
	const { dispatch: storyDispatch, state: { currentUS, loading } } = useContext(UserStoryContext);
	const { hasPermission } = useContext(ProjectRolContext);
	const { state: { activities }} = useContext(ActivityContext);
	const [selectedOption, setSelectedOption] = useState(1);

	// state para manejar la edición del nombre de un US
	const [editName, setEditName] = useState<boolean>(false);

	// state para manejar la edición de la descripción de un US
	const [editDescription, setEditDescription] = useState<boolean>(false);

	// state para abrir form de estimación
	const [openEstimate, setOpenEstimate] = useState<boolean>(false);

	const handleAssign = async () => {
		if (selectedMember)
			await putUserStory(storyDispatch, {
				id: formData.id,
				member: selectedMember.id,
			}, currentProject.id)
	}

	const getMiembroById = useCallback((id: number) => {
		return miembros.find(miembro => miembro.id === id);
	}, [miembros]);

	const handleChange = (field: string, value: string) => {
		setFormData({ ...formData, [field]: value });
	}

	const updateUS = async () => {
		const { id, name, description, estimate } = formData;
		await putUserStory(storyDispatch, {
			id,
			name,
			description,
			estimate,
		}, currentProject.id);
	}

	useEffect(() => {
		setFormData(savedUserStory || currentUS);
		setSelectedMember(getMiembroById((savedUserStory || currentUS).member));
		console.log(currentUS);
	}, [savedUserStory, currentUS, getMiembroById]);

	const classes = useStyles();

	return (
		<Modal
			open={isOpen}
			onClose={handleClose}
			aria-labelledby="form-dialog-title"
			className={classes.modal}
		>
			<Container className={classes.container}>
				{loading ? <LoaderComponent /> : <Box className={classes.content}>
					<Box component="main" className={classes.main}>
						<Box style={{ marginBottom: 20 }}>
							<Typography className={classes.title}>
								Story
							</Typography>
							{
								editName ?
									<TextField
										fullWidth
										autoFocus
										value={formData.name}
										onChange={event => handleChange('name', event.target.value)}
										margin="dense"
										disabled={readOnly}
										id="story-name"
										variant="outlined"
										onKeyPress={async (event) => {
											if (event.key === 'Enter') {
												updateUS();
												setEditName(false);
											}
										}}
										onBlur={async () => {
											updateUS();
											setEditName(false);
										}}
									/> :
									<Box style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
										<Typography className={classes.storyName}>
											{formData.name}
										</Typography>
										{currentProject.status !== 'FINALIZADO' && <PermissionsChecker
											tipo="Project"
											permissions={[ProjectPermissionsCode.MODIFICAR_USER_STORY,]}
										>
											<Edit onClick={() => !readOnly && setEditName(true)} style={{ cursor: 'pointer', color: '#302929' }} />
										</PermissionsChecker>}
									</Box>
							}
						</Box>

						<Box style={{ marginBottom: 40 }}>
							<Typography className={classes.title}>
								Description
							</Typography>
							{
								editDescription ?
									<TextField
										fullWidth
										autoFocus
										value={formData.description}
										onChange={event => handleChange('description', event.target.value)}
										margin="dense"
										multiline
										disabled={currentProject.status === 'FINALIZADO'}
										rows={4}
										id="story-name"
										variant="outlined"
										onBlur={async () => {
											updateUS();
											setEditDescription(false);
										}}
									/> :
									<Typography
										className={classes.storyDescription}
										style={
											hasPermission(ProjectPermissionsCode.MODIFICAR_USER_STORY) ?
												{ cursor: 'pointer' } :
												{}
										}
										onClick={() =>
											!readOnly && hasPermission(ProjectPermissionsCode.MODIFICAR_USER_STORY) && setEditDescription(true)
										}
									>
										{formData.description}
									</Typography>
							}
						</Box>

						<Box style={{
							display: 'flex',
							width: '30%',
							justifyContent: 'space-between'
						}}>
							<Box style={{
								display: 'flex',
								flexDirection: 'column',
								alignItems: 'center',
							}}>
								<Typography style={{
									fontSize: 16,
									color: '#595151',

								}}>
									Asignado
								</Typography>
								{
									!formData.member &&
									<Fab
										disabled={currentProject.status === 'FINALIZADO'}
										style={!hasPermission(ProjectPermissionsCode.ASIGNAR_USER_STORY) ? { cursor: 'default' } : {}}
										size="small" onClick={currentProject.status !== 'FINALIZADO' ? () => !readOnly && hasPermission(ProjectPermissionsCode.ASIGNAR_USER_STORY) && setOpenMiembros(true) : undefined}>
										<AddIcon />
									</Fab>
								}
								{
									formData.member &&
									<Box
										style={hasPermission(ProjectPermissionsCode.ASIGNAR_USER_STORY) ? { cursor: 'pointer' } : {}}
										onClick={() =>
											!readOnly && hasPermission(ProjectPermissionsCode.ASIGNAR_USER_STORY) && setOpenMiembros(true)
										}
									>
										{
											(getMiembroById(formData.member)?.user.picture?.length || 0) > 0 &&
											<Avatar aria-label="recipe" src={getMiembroById(formData.member)?.user.picture} />
										}
										{
											(getMiembroById(formData.member)?.user.picture?.length || 0) === 0 &&
											<Avatar aria-label="recipe">
												{getMiembroById(formData.member)?.user.first_name[0] || '' + getMiembroById(formData.member)?.user}
											</Avatar>
										}
									</Box>
								}
							</Box>
							<Box style={{
								display: 'flex',
								flexDirection: 'column',
								alignItems: 'center',
							}}>
								<Typography style={{
									fontSize: 16,
									color: '#595151',

								}}>
									Estimación
								</Typography>
								<Fab
									disabled={currentProject.status === 'FINALIZADO'}
									size="small"
									onClick={() =>
										!readOnly && hasPermission(ProjectPermissionsCode.ESTIMAR_USER_STORY) && setOpenEstimate(true)
									}
									style={!hasPermission(ProjectPermissionsCode.ESTIMAR_USER_STORY) ? { cursor: 'default' } : {}}
								>
									{
										formData.estimate > 0 ?
											<Typography style={{ fontWeight: 'bold' }}>{formData.estimate}</Typography> :
											<AddIcon />
									}
								</Fab>
							</Box>
							<Box style={{
								display: 'flex',
								flexDirection: 'column',
								alignItems: 'center',
							}}>
								<Typography style={{
									fontSize: 16,
									color: '#595151',

								}}>
									Horas
								</Typography>
								<Fab 
									size="small"
									style={{cursor: 'default'}}
								>
									{
											<Typography style={{fontWeight: 'bold'}}>{activities.map(
												a=>a.worked_hours).reduce((val,acc)=>val+acc, 0)}
											</Typography>
									}
								</Fab>
							</Box>
						</Box>
					</Box>

					<Box component="aside" style={{
						width: '30%',
						backgroundColor: '#f2f2f2',
						padding: 16,
						borderTopRightRadius: 15,
						borderBottomRightRadius: 15,
					}}>
						<Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={selectedOption}
                        label="Sprint"
                        variant="outlined"
                        onChange={event => setSelectedOption(event.target.value as number)}
                    >
                        {
                            options.map(item => (
                                <MenuItem value={item.id}>{item.label}</MenuItem>
                            ))
                        }
                    </Select>
						{options[selectedOption-1].label === 'Comentarios' && <CommentSectionComponent readOnly={readOnly} />}
						{options[selectedOption-1].label === 'Actividades' && <ActivitySectionComponent/>}
						{options[selectedOption-1].label === 'Historial' && <EventUSSectionComponent/>}
					</Box>

					<MemberPickerComponent
						open={openMiembros}
						handleClose={() => setOpenMiembros(false)}
						selected={selectedMember}
						handleSelect={(miembro: Member) => setSelectedMember(miembro)}
						handleAssign={handleAssign}
					/>

					<Dialog open={openEstimate} onClose={() => setOpenEstimate(false)}>
						<DialogTitle>Estimar</DialogTitle>
						<DialogContent>
							<DialogContentText>
								Ingrese la estimación en story points:
							</DialogContentText>
							<TextField
								autoFocus
								value={formData.estimate}
								onChange={event => setFormData({ ...formData, estimate: Number(event.target.value) })}
								margin="dense"
								id="name"
								type="number"
								fullWidth
								variant="outlined"
							/>
						</DialogContent>
						<DialogActions>
							<Button onClick={() => setOpenEstimate(false)}>Cancelar</Button>
							<Button color="primary" onClick={async () => {
								setOpenEstimate(false);
								await updateUS();
							}}>Guardar</Button>
						</DialogActions>
					</Dialog>
				</Box>}
			</Container>
		</Modal>
	);
}

export default UserStoryCardComponent;
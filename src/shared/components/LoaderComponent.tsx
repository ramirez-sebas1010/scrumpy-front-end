import React, { FC } from 'react';
import Loader from 'react-loader-spinner';
import { makeStyles, useTheme } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    loader: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
}));


const LoaderComponent: FC = () => {
    const tema = useTheme();
    const classes = useStyles();
    return (
        <div className={classes.loader}>
            <Loader type="Bars" color={tema.palette.primary.main} height={100} width={100} />
        </div>
    );
}

export default LoaderComponent;

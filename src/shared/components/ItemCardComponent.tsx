import React, { FC } from 'react';
import { Box, makeStyles, Theme, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    cardStyle: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: theme.palette.primary.main,
        color: "#FFFFFF",
        padding: 10,
        borderRadius: 10,
    },
    cardInnerRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
}));

type ItemCardProps = {
    upperLeft?: string;
    lowerLeft?: string;
    upperRight?: string;
    lowerRight?: string;
    marginBottom?: number;
    clickable?: boolean;
    onClick?: () => void;
}

const ItemCardComponent: FC<ItemCardProps> = ( props ) => {
    const classes = useStyles();
    const { upperLeft, lowerLeft, upperRight, lowerRight, marginBottom = 0, clickable = false, onClick } = props;
    return (
        <Box className={classes.cardStyle} style={{marginBottom, cursor: clickable ? 'pointer' : 'default'}} onClick={onClick}>
            <Box className={classes.cardInnerRow}>
                <Typography variant="h6">{ upperLeft }</Typography>
                <Typography variant="h6">{ upperRight }</Typography>
            </Box>
            <Box style={{height: 5}} />
            <Box className={classes.cardInnerRow}>
                <Typography>{ lowerLeft }</Typography>
                <Typography>{ lowerRight }</Typography>
            </Box>
        </Box>
    )
}

export default ItemCardComponent

import { makeStyles, Theme, createStyles, Button, Typography } from '@material-ui/core';
import { FC, useContext } from 'react';

import proyects from "../../assets/images/mis_proyectos.svg";
import { PermissionCode } from '../../data/constants/permisos';
import { RolContext } from '../../data/providers/rol/rol-context';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        myProyects: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly'
        },
        buttonCreateContainer: {
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        buttonCreate: {
            padding: 10,
            paddingLeft: 30,
            paddingRight: 30,
        },
        imgProject: {
            marginTop: 50,
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
        },
    }),
);

type WithOutProjectProjectProps = {
    handleOpen: () => void
}

const WithOutProjectComponent: FC<WithOutProjectProjectProps> = ({ handleOpen }) => {
    const classes = useStyles();
    const { hasPermission } = useContext(RolContext);
    
    return (
        <>
            <div className={classes.imgProject}>
                <img src={proyects} alt="" width="70%" />
            </div>
            <div className={classes.buttonCreateContainer}>
                {
                    hasPermission(PermissionCode.CREAR_PROYECTO) ?
                    <Button 
                    className={classes.buttonCreate}
                    onClick={handleOpen} 
                    variant="contained" 
                    color="primary" 
                    component="span"
                    >
                        Empezar a Crear
                    </Button>:
                <Typography>Aun no estás en ningún proyecto</Typography>
                }
            </div>
        </>
    )
}
export default WithOutProjectComponent;

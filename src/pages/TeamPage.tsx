import React, { useState, useContext, useEffect } from 'react';
import { Box, Theme, makeStyles, createStyles, Typography, Divider, Button, Tooltip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, InputLabel, MenuItem, Select, IconButton, Snackbar, FormHelperText } from '@material-ui/core';
import WithTeamComponent from '../shared/components/WithTeamComponent';
import WithOutTeamComponent from '../shared/components/WithOutTeamComponent';
import MiembroFormComponent from '../shared/components/MiembroFormComponent';
import { MiembroContext, replace } from '../data/providers/miembro/miembro-context';
import { Member, MemberInitializer } from '../shared/interfaces/miembro.interface';
import { postMiembro } from '../data/providers/miembro/miembro-action-creators';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { ProjectRolContext } from '../data/providers/rolProyecto/rolProyecto-context';
import LoaderComponent from '../shared/components/LoaderComponent';
import { Settings } from '@material-ui/icons';
import Close from '@material-ui/icons/Close';
import { SprintContext } from '../data/providers/sprint/sprint-context';
import { UserStoryContext } from '../data/providers/userstory/user-story-context';
import { Usuario, usuarioInitializer } from '../shared/interfaces/usuario.interface';
import UserFormComponent from '../shared/components/UserFormComponent';
import { UsuarioContext } from '../data/providers/usuario/usuario-context';
import userStoryScrumpyAPI from '../api/userStoryScrumpyAPI';
import proyectoApi from '../api/proyectoScrumpyAPI';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        container: {
            display: 'flex',
            flexGrow: 1,
            justifyContent: 'center',
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
            height: '100vh',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

export const TeamPage = () => {
    const classes = useStyles();

    const { state: { userStories }, dispatch: storyDispatch } = useContext(UserStoryContext);
    const { state: { currentSprint }, dispatch: sprintDispatch } = useContext(SprintContext);
    const { state: { usuarios }, dispatch: userDispatch } = useContext(UsuarioContext);

    const { state: { currentProject } } = useContext(ProyectoContext);

    const { state: { roles } } = useContext(ProjectRolContext);

    const { state: { miembros, loading }, dispatch } = useContext(MiembroContext);

    const [openAlertDialog, setOpenAlertDialog] = useState(false);
    const [openSnackbar, setOpenSnackbar] = useState(false);

    const [isReplaceable, setIsReplaceable] = useState(false);

    const [seleccionadoMiembroReemplazado, setSeleccionadoMiembrosReemplazado] = useState<Member>(MemberInitializer);
    const [seleccionadoUserReemplazado, setSeleccionadoUserReemplazado] = useState<Usuario>(usuarioInitializer);
    const [finalSelectUsers, setFinalSelectUsers] = useState<Usuario[]>([]);

    useEffect(() => {
        var a: Usuario[] = [];
        var indexMembers = miembros.map(m => m.user.id);
        console.log("Index de todos los Usuarios del Proyecto");
        console.log(indexMembers);

        //Recorremos todos los usuario del sistema y verificamos si estan en el proyecto
        for (const usu of usuarios) {
            if (!indexMembers.includes(usu.id)) {
                a.push(usu);
            }
        }
        console.log("Todos los Usuarios que no esta en el Proyecto");
        console.log(a);
        if (finalSelectUsers.length > 0) {
            setSeleccionadoUserReemplazado(a[0]);
        }
        setFinalSelectUsers(a);


    }, []);

    useEffect(() => {
        if (miembros.length > 1 && setFinalSelectUsers.length > 0) {
            setIsReplaceable(true);
        } else {
            setIsReplaceable(false);
        }
    }, []);


    const handleClickOpen = () => {
        console.log("Abrir Dialog");
        if (miembros.length > 1 && miembros.length < usuarios.length) {
            setIsReplaceable(true);
            setOpenAlertDialog(true);
        } else {
            setOpenSnackbar(true);
            setIsReplaceable(false);
        }
        console.log(openAlertDialog);
        console.log(isReplaceable);

    };

    const handleClickOpenSnackbar = () => {
        setOpenSnackbar(true);
    };

    const handleClickCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    const handleChangeR1 = (event: React.ChangeEvent<{ name?: string | undefined; value: unknown; }>) => {
        console.log(event.target.value);
        const miembro = miembros.find(m => m.id === event.target.value);
        console.log(miembro);
        if (miembro) {
            console.log("event.target");
            setSeleccionadoMiembrosReemplazado({ ...miembro });
        }
    };

    const handleChangeR2 = (event: React.ChangeEvent<{ name?: string | undefined; value: unknown; }>) => {
        console.log(event.target.value);
        const miembro = miembros.find(m => m.id === event.target.value);
        const usuario = usuarios.find(m => m.id === event.target.value);
        console.log(event.target);
        if (usuario) {
            console.log("event.target");
            setSeleccionadoUserReemplazado({ ...usuario });
        }

    };

    const reemplazarUsuario = async () => {
        console.log("reemplazarUsuario");
        console.log(seleccionadoUserReemplazado);
        console.log(seleccionadoMiembroReemplazado);
        try {
            await proyectoApi.reeplazarUsuario(currentProject.id, seleccionadoMiembroReemplazado.id, seleccionadoUserReemplazado.id);
            handleClickClose();
        } catch (error) {
            handleClickClose();
            console.log(error);
        }

    };


    const handleClickClose = () => {
        dispatch({ type: 'SWITCH_MIEMBRO', payload: { seleccionadoUserReemplazado, seleccionadoMiembroReemplazado } as replace });
        setOpenAlertDialog(false);
    };

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSave = async (miembro: Member) => {
        postMiembro(dispatch, miembro, currentProject.id as number);
    }

    const action = (
        <React.Fragment>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <Close fontSize="small" />
            </IconButton>
        </React.Fragment>
    );
    console.log('sissisisi');
    console.log(seleccionadoUserReemplazado);
    console.log(seleccionadoMiembroReemplazado);
    console.log('nonoononono');
    console.log(isReplaceable || finalSelectUsers.length > 0);

    return (
        !loading ?
            <Box component="main" className={classes.content}>
                <Box style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Typography variant="h2" >Equipo</Typography>
                    {currentProject.status !== "FINALIZADO" && <Tooltip title="Reemplazar Un Miembro"><Button onClick={handleClickOpen}><Settings fontSize="large" /></Button></Tooltip>}
                </Box>
                <br />
                <Divider></Divider>
                <br />
                {
                    miembros.length !== 0 ?
                        <WithTeamComponent /> :
                        <WithOutTeamComponent handleOpen={handleOpen} />
                }
                <MiembroFormComponent open={open} rolList={roles} save={handleSave} cerrarModal={handleClose} />
                {
                    (isReplaceable) ?
                        <Dialog
                            open={openAlertDialog}
                            keepMounted
                            onClose={() => setOpenAlertDialog(false)}
                            aria-labelledby="alert-dialog-slide-title"
                            aria-describedby="alert-dialog-slide-description"
                        >
                            <DialogTitle id="alert-dialog-slide-title">{"Reemplazo de Equipo"}</DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">
                                    <Typography>Seleccione los Miembros que seran Reemplazados</Typography>
                                    <Box style={{ display: 'flex', width: '100%' }}>
                                        <Box>
                                            <Box>
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="demo-simple-select-label">Reemplazado</InputLabel>
                                                    <Select
                                                        fullWidth
                                                        value={seleccionadoMiembroReemplazado.id}
                                                        onChange={handleChangeR1}
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        label="Miembro a Ser Reemplazado"
                                                    >
                                                        {miembros.filter(m => m.projectRole.name !== "Scrum master").map(member =>
                                                            <MenuItem key={member.id} value={member.id}>{member.user.email}</MenuItem>
                                                        )}
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                            <Box>
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="demo-simple-select-label">Reemplazo</InputLabel>
                                                    <Select
                                                        fullWidth
                                                        value={seleccionadoUserReemplazado.id}
                                                        onChange={handleChangeR2}
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        label="Miembro a Ser Reemplazado"
                                                    >
                                                        {finalSelectUsers.map(user =>
                                                            <MenuItem key={user.id} value={user.id}>{user.email}</MenuItem>
                                                        )}
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </Box>

                                    </Box>
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={() => {
                                    setOpenAlertDialog(false);
                                    setSeleccionadoMiembrosReemplazado(MemberInitializer);
                                    setSeleccionadoUserReemplazado(usuarioInitializer);
                                }} color="primary">
                                    Cancelar
                                </Button>
                                <Button onClick={reemplazarUsuario} color="primary">
                                    Confirmar
                                </Button>
                            </DialogActions>
                        </Dialog> : <Snackbar
                            open={openSnackbar}
                            autoHideDuration={6000}
                            onClose={handleClickCloseSnackbar}
                            message="Debe haber mas de un Miembro sin incluir al Scrum Master o No hay usuario disponible"
                            action={action}
                        />

                }
            </Box> :
            <LoaderComponent />
    );
}
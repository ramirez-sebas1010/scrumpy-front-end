import { useContext, useEffect, useState } from 'react';
import { Box, Typography, Divider, Theme, makeStyles, createStyles, TextField, Button } from '@material-ui/core';
import LoaderComponent from '../shared/components/LoaderComponent';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { Proyecto, proyectoInitializer } from '../shared/interfaces/proyecto.interface';
import { putProyecto } from '../data/providers/proyecto/proyecto-action-creators';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { ProjectPermissionsCode } from '../data/constants/permisos';
import { ProjectRolContext } from '../data/providers/rolProyecto/rolProyecto-context';

const drawerWidth = 240;
const MARGIN_BOTTOM = 30;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        drawerPaper: {
            width: drawerWidth,
        },
        toolbar: theme.mixins.toolbar,
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
        root: {
            display: 'flex',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        button: {
            backgroundColor: theme.palette.primary.main,
            marginBottom: 15,
            marginTop: 30,
            color: '#FFFFFF',
            padding: 10,
        },
    }),
);

export const ConfiguracionesPage = () => {
    const classes = useStyles();

    const [confData, setConfData] = useState<Proyecto>(proyectoInitializer);

    const { state: { currentProject, loading }, dispatch } = useContext(ProyectoContext)

    const { hasPermission } = useContext(ProjectRolContext);
    useEffect(() => setConfData(currentProject), [currentProject]);

    return (
        !loading ?
            <Box component="main" className={classes.content}>
                <div>
                    <Typography style={{ textAlign: 'left' }} variant="h2" >Configuraciones</Typography>
                </div>
                <br />
                <Divider></Divider>
                <br />
                <br />
                <TextField
                    value={confData.name}
                    onChange={(event) => setConfData({ ...confData, name: event.target.value })}
                    required
                    margin="dense"
                    id="rolName"
                    label="Nombre"
                    placeholder="Nombre del Proyecto"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    style={{ marginBottom: MARGIN_BOTTOM }}
                    disabled={!hasPermission(ProjectPermissionsCode.EDITAR_LAS_CONFIGURACIONES) || currentProject.status === 'FINALIZADO'}
                />
                <TextField
                    value={confData.description}
                    onChange={(event) => setConfData({ ...confData, description: event.target.value })}
                    margin="dense"
                    id="rolDescription"
                    label="Descripción"
                    placeholder="Descripción del Proyecto"
                    multiline
                    rows={4}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    style={{ marginBottom: MARGIN_BOTTOM }}
                    disabled={!hasPermission(ProjectPermissionsCode.EDITAR_LAS_CONFIGURACIONES) || currentProject.status === 'FINALIZADO'}
                />
                <TextField
                    value={confData?.sprintDuration}
                    onChange={(event) => setConfData({ ...confData, sprintDuration: Number(event.target.value) })}
                    margin="dense"
                    id="rolSprintDuration"
                    label="Duración de sprints"
                    placeholder="Duración de los sprints del proyecto en semanas"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    type="number"
                    disabled={!hasPermission(ProjectPermissionsCode.EDITAR_LAS_CONFIGURACIONES) || currentProject.status === 'FINALIZADO'}
                />
                <Box style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                }}>
                    {currentProject.status !== 'FINALIZADO' && <PermissionsChecker
                        tipo="Project"
                        permissions={[ProjectPermissionsCode.EDITAR_LAS_CONFIGURACIONES]}
                    >
                        <Button
                            className={classes.button}
                            onClick={() => putProyecto(dispatch, confData)}
                        >
                            Guardar
                        </Button>
                    </PermissionsChecker>
                    }
                </Box>
            </Box> :
            <LoaderComponent />
    );
}

export default ConfiguracionesPage;

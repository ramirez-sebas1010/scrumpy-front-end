
import { AppBar, Avatar, Box, Button, Container, createStyles, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, LinearProgress, Link, makeStyles, Theme, Toolbar, Typography } from '@material-ui/core';
import Slide from '@material-ui/core/Slide';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { useContext, useEffect, useState } from 'react';
import { putProyecto } from '../data/providers/proyecto/proyecto-action-creators';
import proyects from "../assets/images/mis_proyectos.svg";
import { SprintContext } from '../data/providers/sprint/sprint-context';
import { postSprint, putSprint, setCurrentSprint, setSelectedSprint } from '../data/providers/sprint/sprint-action-creators';
import { Sprint } from '../shared/interfaces/sprint.interface';
import { UserStoryContext } from '../data/providers/userstory/user-story-context';
import DialogComponent from '../shared/components/DialogComponent';
import { ErrorContext } from '../data/providers/error/error-context';
import { Proyecto } from '../shared/interfaces/proyecto.interface';
import { MiembroContext } from '../data/providers/miembro/miembro-context';
import { PlanningBoard } from '../shared/components/PlanningBoard';
import { putUserStory } from '../data/providers/userstory/user-story-action-creators';
import { UserStory } from '../shared/interfaces/user_story.interface';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import LoaderComponent from '../shared/components/LoaderComponent';
import React from 'react';
import PermissionsChecker from '../shared/components/PermissionsChecker';
import { ProjectPermissionsCode } from '../data/constants/permisos';
import dayjs from 'dayjs';
import TestPDFPage from './TestPDFPage';
import { PDFDownloadLink } from '@react-pdf/renderer';
import activityScrumpyAPI from '../api/activityScrumpyAPI';
import ReporteSprintComponent from '../shared/components/ReporteSprintComponent';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            height: '100vh',
            width: '100vw',
        },
        buttonCreate: {
            height: '50px',
            display: 'flex',
            justifyContent: 'center',
        },
        imgTeam: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.default,
            padding: theme.spacing(3),
        },
        namePro: {
            marginBottom: '20px',
        },
        title: {
            flexGrow: 1,
        },
        navBar: {
            marginBottom: '10px'
        }
    }),
);



const PlanningPage = () => {
    const classes = useStyles();
    const { state: { currentProject, loading }, dispatch } = useContext(ProyectoContext);
    const { state: { userStories }, dispatch: storyDispatch } = useContext(UserStoryContext);
    const { state: { currentSprint }, dispatch: sprintDispatch } = useContext(SprintContext);
    const { state: { miembros } } = useContext(MiembroContext);
    const { dialog, setDialog, handleClose } = useContext(ErrorContext);
    const history = useHistory();

    const [openAlertDialog, setOpenAlertDialog] = useState(false);
    const [openAlertDialogFinalizeProject, setOpenAlertDialogFinalizeProject] = useState(false);
    const [finalSprint, setFinalSprint] = useState(false);

    const handleClickFinalizeProject = () => {
        setOpenAlertDialogFinalizeProject(true);
    }

    const handleCloseFinalizeProject = () => {
        setOpenAlertDialogFinalizeProject(false);
    }


    const handleClickOpen = () => {
        setOpenAlertDialog(true);
    };

    const handleClickClose = () => {
        setOpenAlertDialog(false);

    };

    const [productBacklog, setProductBacklog] = useState<UserStory[]>([]);
    const [sprintBacklog, setSprintBacklog] = useState<UserStory[]>([]);

    const [storiesForReport, setStoriesForReport] = useState<UserStory[]>([]);


    useEffect(() => {
        let dataPB;
        if (!currentSprint.status || currentSprint.status === "FINALIZADO") {
            dataPB = userStories;
        } else {
            dataPB = userStories.filter(us => us.sprint === null || (us.sprint as any).iteration !== currentSprint.iteration);
        }
        setProductBacklog(dataPB);
        setSprintBacklog(userStories.filter(us => us.sprint !== null).filter(us => (us.sprint as any).iteration === currentSprint.iteration));
    }, [userStories, currentSprint]);

    useEffect(() => {
        (async () => {
            let reportData = [];
            const storiesData = userStories.filter(us => us.sprint !== null).filter(us => (us.sprint as any).iteration === currentSprint.iteration);
            console.log(currentProject.name)
            for(let story of storiesData) {
                const actividades = await activityScrumpyAPI.getAllActivities(currentProject.id, story.id);
                let  horas = actividades
                    .map(activity => activity.worked_hours)
                    .reduce((suma, horas) => suma + horas, 0);

                reportData.push({...story, worked_hours: horas});
            }
            console.log(reportData)
            setStoriesForReport(reportData);
        })();
    }, [currentSprint]);

    // Si el sprint es null el US va al Product Backlog
    //Si el sprint es distinto a null el US va al Sprint Backlog
    const startProject = async () => {
        try {
            let proyecto: Proyecto = { ...currentProject, status: "INICIADO", startDay: new Date().toISOString().slice(0, 10) };
            await putProyecto(dispatch, proyecto, true);
        } catch (error: any) {
            alert(error.message);
        }
    }

    //create sprint
    const createSprint = async () => {
        try {
            let newSprint: Partial<Sprint> = {
                sprint_goal: 'Por defecto',
                status: 'EN_PLANIFICACION',
            }
            let savedSprint = await postSprint(sprintDispatch, newSprint, currentProject.id as number);
            savedSprint && sprintDispatch(setCurrentSprint(savedSprint));
        } catch (error: any) {
            alert(error.message);
        }
    }

    const startSprint = async () => {
        if (sprintBacklog.length > 0) {

            const storyUnassigned = sprintBacklog.filter((story => !story.member));
            const ids = storyUnassigned.map(story => `#${story.id}`);
            const idsWithFormat = ids.join(", ");

            if (storyUnassigned.length > 0) {
                setDialog({
                    title: 'Atención ⚠️',
                    body: `Para iniciar un sprint tus stories ${idsWithFormat} deben estar asignados!`,
                    buttonLabel: 'Aceptar',
                    open: true
                });
                return;
            }
            try {
                let newSprint: Sprint = { ...currentSprint, status: 'INICIADO', startDay: dayjs().format("YYYY-MM-DD") };
                await putSprint(sprintDispatch, newSprint, currentProject.id, currentSprint.id);
                for (let story of sprintBacklog) {
                    await putUserStory(storyDispatch, {
                        id: story.id,
                        status: 'TODO',
                    }, currentProject.id);
                }

                sprintDispatch(setSelectedSprint(newSprint.id));

                //Navegar a Sprint Page
                history.push(`/projectDetail/${currentProject.id}/sprint`);

            } catch (error: any) {
                alert(error.message);
            }
        } else {
            setDialog({
                title: 'Atención ⚠️',
                body: 'No puedes iniciar un sprint con el sprint backlog vacío!',
                buttonLabel: 'Aceptar',
                open: true
            });
        }
    }

    const finalizarSprint = async () => {
        try {
            let sprint: Sprint = { ...currentSprint, status: 'FINALIZADO', endDay: dayjs().format("YYYY-MM-DD") };
            await putSprint(sprintDispatch, sprint, currentProject.id as number, currentSprint.id as number);

            for await (const us of userStories) {
                if (us.status !== 'DONE' && us.status !== 'ARCHIVED' && us.status !== 'NEW') {
                    putUserStory(storyDispatch,
                        { id: us.id, project: (us.project as any).id, sprint: null, status: 'UNFINISHED', priority: us.priority != 10 ? us.priority + 1 : 10 }
                        , currentProject.id);

                }
            }

        } catch (error: any) {
            alert(error.message);
        }
        console.log(currentProject.status);
    }

    const finalizarProyecto = async () => {
        if (currentSprint.status !== 'INICIADO') {
            console.log(currentSprint.status);
            if (currentProject.status !== "FINALIZADO") {
                handleClickFinalizeProject();
                return;
            }
            setDialog({
                title: 'Atención ⚠️',
                body: 'No se puede Finalizar un Proyecto ya finalizado',
                buttonLabel: 'Aceptar',
                open: true
            });
            return;
        } else {
            setDialog({
                title: 'Atención ⚠️',
                body: 'No se puede Finalizar un Proyecto con un Sprint en Curso',
                buttonLabel: 'Aceptar',
                open: true
            });
        }
    }






    const getTeamCapacity = () => {
        let capacity = 0;

        miembros.forEach(miembro => {
            capacity += miembro.is_active ? miembro.availability : 0;
        });

        return capacity;
    }

    const getEstimatedHours = () => {
        let estimated = 0;
        if (currentSprint.status === 'FINALIZADO') return 0;
        sprintBacklog.forEach(story => {
            estimated += story.estimate;
        })
        return estimated;
    }

    if (currentProject.status === 'NEW') {
        return (
            <>
                <Container style={{ height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly', alignContent: 'center' }}>
                    <Container className={classes.imgTeam}>
                        <img src={proyects} alt="" width="50%" />
                    </Container>
                    <Container className={classes.buttonCreate}>
                        <Button onClick={startProject} variant="contained" color="primary" component="span">
                            Iniciar Proyecto
                        </Button>
                    </Container>

                </Container>
            </>
        );
    }

    let boton;
    if (currentSprint.status === '') {
        boton = (
            <PermissionsChecker
                tipo="Project"
                permissions={[ProjectPermissionsCode.CREAR_SPRINT]}
            >
                <Button disabled={currentProject.status === "FINALIZADO"} onClick={createSprint} variant="contained" color="primary" component="span">
                    CREAR SPRINT
                </Button>
            </PermissionsChecker>
        )

    } else if (currentSprint.status === 'EN_PLANIFICACION') {
        boton = (
            <PermissionsChecker
                tipo="Project"
                permissions={[ProjectPermissionsCode.INICIAR_SPRINT]}
            >
                <Button disabled={currentProject.status === "FINALIZADO"} onClick={startSprint} variant="contained" color="primary" component="span">
                    INICIAR SPRINT
                </Button>
            </PermissionsChecker>
        )

    } else if (currentSprint.status === 'INICIADO') {
        boton = (
            <PermissionsChecker
                tipo="Project"
                permissions={[ProjectPermissionsCode.FINALIZAR_SPRINT]}
            >
                <Button disabled={currentProject.status === "FINALIZADO"} onClick={handleClickOpen} variant="contained" color="primary" component="span">
                    FINALIZAR SPRINT
                </Button>
            </PermissionsChecker>
        )
    } else if (currentSprint.status === 'FINALIZADO') {
        boton = (
            <PermissionsChecker
                tipo="Project"
                permissions={[ProjectPermissionsCode.CREAR_SPRINT]}
            >
                <Button disabled={currentProject.status === "FINALIZADO"} onClick={createSprint} variant="contained" color="primary" component="span">
                    CREAR SPRINT
                </Button>
            </PermissionsChecker>
        )
    }

    return (
        loading ?
            <LoaderComponent /> :
            <>
                <Container style={{ width: '100%' }}>
                    <Box style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '30px' }}>
                        <Typography variant="h2">Mi Planning</Typography>
                        {currentProject.status !== 'FINALIZADO' && <Button onClick={finalizarProyecto} variant="contained" color="primary" component="span">
                            FINALIZAR PROYECTO
                        </Button>
                        }
                        {currentProject.status !== 'FINALIZADO' && boton}


                    </Box>
                    <Box>
                        <Typography className={classes.namePro} variant="h6">Proyecto {currentProject.status === "FINALIZADO" ? "Finalizado":""}: {currentProject.name}</Typography>
                        {currentProject.status === "FINALIZADO" && <Typography className={classes.namePro} variant="h6">Deadline del proyecto: {currentProject.endDay}</Typography>}
                    </Box>
                    <Box style={{marginBottom: 10}}>
                        <Typography>Capacidad utilizada: {getEstimatedHours()} / {getTeamCapacity()} </Typography>
                        <LinearProgress
                            variant="determinate"
                            value={
                                getEstimatedHours() < getTeamCapacity() ?
                                    (getEstimatedHours() / getTeamCapacity()) * 100 :
                                    100
                            }
                            color={getEstimatedHours() > getTeamCapacity() ? 'secondary' : 'primary'}
                        />
                    </Box>
                    <PDFDownloadLink
                        document={<TestPDFPage stories={userStories} />} 
                        fileName={"backlog"}
                    > 

                        <button> Reporte  backlog </button> 

                    </PDFDownloadLink> 
                    <PDFDownloadLink
                        document={<ReporteSprintComponent stories={storiesForReport} />} 
                        fileName={"sprint"}
                    > 

                        <button> Reporte Sprint </button> 

                    </PDFDownloadLink> 
                    <PlanningBoard sprintStatus={currentSprint.status} sprintBacklog={sprintBacklog} productBacklog={productBacklog} setProductBacklog={setProductBacklog} setSprintBacklog={setSprintBacklog} />
                    
                    <DialogComponent dialog={dialog} handleClose={handleClose} />
                </Container>
                <Dialog
                    open={openAlertDialog}
                    keepMounted
                    onClose={handleClickClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">{"WARNING"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Estas Seguro que quieres finalizar el sprint?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClickClose} color="primary">
                            NO
                        </Button>
                        <Button onClick={async () => {
                            setFinalSprint(true);
                            await finalizarSprint();
                            await finalizarSprint();
                            handleClickClose();
                        }} color="primary">
                            SI
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={openAlertDialogFinalizeProject}
                    keepMounted
                    onClose={handleCloseFinalizeProject}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">{"WARNING"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Estas Seguro que quieres finalizar el proyecto?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseFinalizeProject} color="primary">
                            NO
                        </Button>
                        <Button onClick={async () => {
                            const proyectoAFinalizar = { ...currentProject, status: "FINALIZADO" };
                            try {
                                await putProyecto(dispatch, proyectoAFinalizar, true);
                            } catch (error) {
                                throw error;
                            }
                            handleCloseFinalizeProject()
                        }} color="primary">
                            SI
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
    )
}


export default PlanningPage

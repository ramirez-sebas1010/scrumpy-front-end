import { Box, Button, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, InputLabel, MenuItem, Select, Typography } from '@material-ui/core'
import { SetStateAction, useCallback, useContext, useEffect, useState } from 'react'
import { DragDropContext, DropResult, ResponderProvided } from 'react-beautiful-dnd'
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context'
import { SprintContext } from '../data/providers/sprint/sprint-context'
import { UserStoryContext } from '../data/providers/userstory/user-story-context'
import TableroComponent from '../shared/components/TableroComponent'
import { UserStory } from '../shared/interfaces/user_story.interface'
import scrum from "../assets/images/scrum_page_illus.svg";
import { putUserStory, setCurrentUS } from '../data/providers/userstory/user-story-action-creators'
import LoaderComponent from '../shared/components/LoaderComponent';
import UserStoryCardComponent from '../shared/components/UserStoryCardComponent'
import { MiembroContext } from '../data/providers/miembro/miembro-context'
import { AuthContext } from '../data/providers/auth/auth-context'
import { ErrorContext } from '../data/providers/error/error-context'
import DialogComponent from '../shared/components/DialogComponent'
import { Link } from 'react-router-dom';
import { setSelectedSprint } from '../data/providers/sprint/sprint-action-creators'
import sprintScrumpyAPI from '../api/sprintScrumpyAPI'
import { PastSprints } from '../shared/interfaces/past-sprints.interface'
import { Usuario } from '../shared/interfaces/usuario.interface'
import dayjs from 'dayjs'



const SprintPage = () => {
    const { state: { auth: { usuario }, } } = useContext(AuthContext);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { miembros } } = useContext(MiembroContext);
    const { state: { userStories, loading }, dispatch: storyDispatch } = useContext(UserStoryContext);
    const { state: { currentSprint, sprints, selectedSprint }, dispatch: sprintDispatch } = useContext(SprintContext);

    const { dialog, setDialog, handleClose: handleCloseDialogError } = useContext(ErrorContext);


    const [todo, setTodo] = useState<UserStory[]>([]);
    const [doing, setDoing] = useState<UserStory[]>([]);
    const [done, setDone] = useState<UserStory[]>([]);
    const [testing, setTesting] = useState<UserStory[]>([]);
    const [openUSCard, setOpenUSCard] = useState<boolean>(false);
    const [currentDate, setCurrentdate] = useState(new Date(Date.now()));
    const [openFinDialog, setOpenFinDialog] = useState<boolean>(false);
    const [pastSprintStories, setPastSprintStories] = useState<PastSprints[]>([]);

    useEffect(() => {
        setTodo(userStories.filter(us => us.sprint !== null && (us.sprint as any).iteration === currentSprint.iteration && us.status === 'TODO'));
        setDoing(userStories.filter(us => us.sprint !== null && (us.sprint as any).iteration === currentSprint.iteration && us.status === 'DOING'));
        setDone(userStories.filter(us => us.sprint !== null && (us.sprint as any).iteration === currentSprint.iteration && us.status === 'DONE'));
        setTesting(userStories.filter(us => us.sprint !== null && (us.sprint as any).iteration === currentSprint.iteration && us.status === 'TESTING'));
    }, [userStories, currentSprint]);

    let daysToMs = (days: number) => days * 24 * 60 * 60 * 1000;

    const calculateDaysFromDate = () => {
        console.log(currentSprint.startDay);
        let dias = currentProject.sprintDuration * 7;

        let finalDate = dayjs(currentSprint.startDay).toDate().getTime() + daysToMs(dias);

        const diffTime = Math.abs(Date.now() - finalDate);

        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        return diffDays;
    }

    useEffect(() => {
        setCurrentdate(new Date(Date.now()));
        let diasRestantes = calculateDaysFromDate();
        if (diasRestantes <= 0) setOpenFinDialog(true);
    }, []);

    useEffect(() => {
        (async () => {
            if (currentSprint.status === 'FINALIZADO' || currentSprint.id !== selectedSprint) {
                let stories = await sprintScrumpyAPI.getPastSprintState(currentProject.id, selectedSprint);
                let uniqueStories: PastSprints[] = [];

                for (let story of stories) {
                    let aux = uniqueStories.filter(unique => unique.user_story.id === story.user_story.id);
                    if (aux.length === 0) uniqueStories.push(story);
                }

                setPastSprintStories(uniqueStories);
                console.log(stories);
                console.log(uniqueStories);
            }
        })();
    }, [selectedSprint]);

    const currentMember = miembros.find(miembro => miembro.user.id === usuario.id);

    const isMyUs = (userStory: UserStory): boolean => userStory.member === currentMember!.id ? true : false;

    const showDialog = (message: string) => {
        setDialog({
            title: 'Atención ⚠️',
            body: message,
            buttonLabel: 'Aceptar',
            open: true
        });
    }

    function handleOnDragEnd(result: DropResult, provided: ResponderProvided) {
        const { destination, source } = result;

        if (currentProject.status !== 'FINALIZADO') {
            //Verifican si esta en el mismo tablero
            if (destination?.droppableId === source.droppableId) {
                console.log('Moviendo en la Misma Lista');
                return
            }

            interface HandlerProperties {
                sourceList: UserStory[];
                destinationList: UserStory[];
                setSourceList: (value: SetStateAction<UserStory[]>) => void;
                setDestinationList: (value: SetStateAction<UserStory[]>) => void;
                newStatus: string;
            }

            let properties: HandlerProperties = {
                sourceList: [],
                destinationList: [],
                setSourceList: (value: SetStateAction<UserStory[]>) => { },
                setDestinationList: (value: SetStateAction<UserStory[]>) => { },
                newStatus: '',
            };

            switch (source?.droppableId) {
                case 'todo':
                    properties.sourceList = [...todo];
                    properties.setSourceList = setTodo;
                    break;
                case 'doing':
                    properties.sourceList = [...doing];
                    properties.setSourceList = setDoing;
                    break;
                case 'qa':
                    properties.sourceList = [...testing];
                    properties.setSourceList = setTesting;
                    break;
                case 'done':
                    properties.sourceList = [...done];
                    properties.setSourceList = setDone;
                    break;
            }

            switch (destination?.droppableId) {
                case 'todo':
                    properties.destinationList = [...todo];
                    properties.setDestinationList = setTodo;
                    properties.newStatus = 'TODO';
                    break;
                case 'doing':
                    properties.destinationList = [...doing];
                    properties.setDestinationList = setDoing;
                    properties.newStatus = 'DOING';
                    break;
                case 'qa':
                    properties.destinationList = [...testing];
                    properties.setDestinationList = setTesting;
                    properties.newStatus = 'TESTING';
                    break;
                case 'done':
                    properties.destinationList = [...done];
                    properties.setDestinationList = setDone;
                    properties.newStatus = 'DONE';
                    break;
            }

            //Generic handler
            const newSourceList = [...properties.sourceList];
            const newDestinationList = [...properties.destinationList];
            const [removed] = newSourceList.splice(source.index, 1);

            if (selectedSprint !== currentSprint.id) {
                showDialog("No se puede mover los US. Tablero de sólo lectura.");
                return;
            }

            if (properties.newStatus === "TESTING") {
                if (usuario.id !== (currentProject.scrum_master as Usuario).id) {
                    showDialog("Usted no puede mover este US a QA");
                    return;
                }
            }

            if (usuario.id === (currentProject.scrum_master as Usuario).id) {
                if (!(removed.status === "DONE" || removed.status === "TESTING")) {
                    if (!isMyUs(removed)) {
                        showDialog("Usted no puede mover este US");
                        return;
                    }
                } else {
                    if (properties.newStatus === "TODO" || properties.newStatus === "DOING") {
                        if (!isMyUs(removed)) {
                            showDialog("Usted no puede mover este US a: " + properties.newStatus);
                            return;
                        }
                    }
                }
            } else {
                if (!isMyUs(removed)) {
                    showDialog("Usted no puede mover este US");
                    return;
                }
            }

            newDestinationList.splice(destination?.index as number, 0, removed);
            putUserStory(storyDispatch, { id: removed.id, project: (removed.project as any).id, sprint: (removed.sprint as any).id, status: properties.newStatus }, currentProject.id);

        } else {
            showDialog("No puedes mover un US de un Proyecto ya finalizado");
        }
    }

    const handleClickUS = (item: UserStory) => {
        storyDispatch(setCurrentUS(item));
        setOpenUSCard(true);
    }

    if ((currentSprint.status === 'EN_PLANIFICACION' || currentSprint.status === '') && sprints?.length <= 1) {
        return (
            <Container style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center' }}>
                <Typography variant="h4" style={{ textAlign: 'center' }}>
                    No hay Sprint Activo
                </Typography>
                <Box style={{ display: 'flex', justifyContent: 'center', alignContent: 'center' }}>
                    <img src={scrum} alt="Scrum Inactivo" style={{ width: '65%' }} />
                </Box>
            </Container>
        );
    }

    if ((currentSprint.id === selectedSprint) && currentSprint.status !== 'FINALIZADO') {
        console.log('Entra')
        return (
            loading ?
                <LoaderComponent /> :
                <>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={currentSprint.id}
                        label="Sprint"
                        variant="outlined"
                        onChange={event => sprintDispatch(setSelectedSprint(event.target.value as number))}
                    >
                        {
                            sprints
                                .filter(sprint => sprint.status !== 'EN_PLANIFICACION')
                                .map(sprint => (
                                    <MenuItem value={sprint.id}>{`Sprint ${sprint.iteration}`}</MenuItem>
                                ))
                        }
                    </Select>
                    <Typography variant="h6">Dias Faltantes para terminar el sprint: {calculateDaysFromDate()}</Typography>
                    <Divider />
                    <DragDropContext onDragEnd={handleOnDragEnd}>
                        <div style={{ display: 'flex', width: '100%' }}>
                            <TableroComponent title="Todo" id='todo' listTiles={todo} handleClick={handleClickUS}></TableroComponent>
                            <TableroComponent title="Doing" id='doing' listTiles={doing} handleClick={handleClickUS}></TableroComponent>
                            <TableroComponent title="Done" id='done' listTiles={done} handleClick={handleClickUS}></TableroComponent>
                            <TableroComponent title="QA" id='qa' listTiles={testing} handleClick={handleClickUS}></TableroComponent>
                        </div>
                    </DragDropContext>
                    <UserStoryCardComponent
                        isOpen={openUSCard}
                        handleClose={() => setOpenUSCard(false)}
                        readOnly={currentSprint.status === 'FINALIZADO'}
                    />
                    <DialogComponent dialog={dialog} handleClose={handleCloseDialogError} />
                    <Dialog
                        open={openFinDialog}
                        keepMounted
                        onClose={() => setOpenFinDialog(false)}
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"
                    >
                        <DialogTitle id="alert-dialog-slide-title">Ya se ha terminado el Sprint</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-slide-description">
                                Deseas Finalizar el Sprint?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => setOpenFinDialog(false)} color="primary">
                                No
                            </Button>
                            <Link to={`/projectDetail/${currentProject.id}/planning`}>
                                <Button color="primary">
                                    Si
                                </Button>
                            </Link>
                        </DialogActions>
                    </Dialog>
                </>
        );
    }
    return (
        <>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedSprint}
                label="Sprint"
                variant="outlined"
                onChange={event => sprintDispatch(setSelectedSprint(event.target.value as number))}
            >
                {
                    sprints
                        .filter(sprint => sprint.status !== 'EN_PLANIFICACION')
                        .map(sprint => (
                            <MenuItem value={sprint.id}>{`Sprint ${sprint.iteration}`}</MenuItem>
                        ))
                }
            </Select>

            <DragDropContext onDragEnd={handleOnDragEnd}>
                <div style={{ display: 'flex', width: '100%' }}>
                    <TableroComponent title="Todo" id='todo' listTiles={pastSprintStories.filter(past => past.story_status === 'TODO').map(past => past.user_story)} handleClick={handleClickUS}></TableroComponent>
                    <TableroComponent title="Doing" id='doing' listTiles={pastSprintStories.filter(past => past.story_status === 'DOING').map(past => past.user_story)} handleClick={handleClickUS}></TableroComponent>
                    <TableroComponent title="QA" id='qa' listTiles={pastSprintStories.filter(past => past.story_status === 'TESTING').map(past => past.user_story)} handleClick={handleClickUS}></TableroComponent>
                    <TableroComponent title="Done" id='done' listTiles={pastSprintStories.filter(past => past.story_status === 'DONE').map(past => past.user_story)} handleClick={handleClickUS}></TableroComponent>
                </div>
            </DragDropContext>
            <UserStoryCardComponent
                isOpen={openUSCard}
                handleClose={() => setOpenUSCard(false)}
                readOnly={true}
            />
            <DialogComponent dialog={dialog} handleClose={handleCloseDialogError} />
            <Dialog
                open={openFinDialog}
                keepMounted
                onClose={() => setOpenFinDialog(false)}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">Ya se ha terminado el Sprint</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        Deseas Finalizar el Sprint?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenFinDialog(false)} color="primary">
                        No
                    </Button>
                    <Link to={`/projectDetail/${currentProject.id}/planning`}>
                        <Button color="primary">
                            Si
                        </Button>
                    </Link>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default SprintPage;

import React, { FC, useContext, useEffect, useState } from 'react';
import { Box, Typography, Divider, Theme, makeStyles, createStyles, Card, CardContent, CardActions, Button, CardHeader, IconButton } from '@material-ui/core';
import WithoutTaskComponent from '../shared/components/WithoutTaskComponent';
import { UsuarioContext } from '../data/providers/usuario/usuario-context';
import { UserStory, userStoryInitializer } from '../shared/interfaces/user_story.interface';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { ProyectoContext } from '../data/providers/proyecto/proyecto-context';
import { AuthContext } from '../data/providers/auth/auth-context';
import authAPI from '../api/authScrumpyAPI';
import LoaderComponent from '../shared/components/LoaderComponent';
import UserStoryCardComponent from '../shared/components/UserStoryCardComponent';
import { UserStoryContext } from '../data/providers/userstory/user-story-context';
import { setCurrentUS } from '../data/providers/userstory/user-story-action-creators';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        myProyects: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly'
        },
        showingProjectCards: {
            display: 'flex',
            flexWrap: 'wrap',
            width: '100%'
        },
        buttonCreate: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '80px'
        },
        imgProject: {
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            height: '100vh',
            position: 'relative',
        },
        addButton: {
            position: 'fixed',
            bottom: 30,
            right: 30,
        },
    }),
);

type WithTaskProps = {
    tasks: UserStory[];
}

const WithTaskComponent: FC<WithTaskProps> = ({ tasks }) => {

    const { state: { proyectos } } = useContext(ProyectoContext);
    const { dispatch } = useContext(UserStoryContext);
    const [savedUserStory, setSavedUserStory] = useState<UserStory>(userStoryInitializer);
    const [open, setOpen] = useState<boolean>(false);

    const groupTasksByProject = (stories: UserStory[]) => {
        // Obtenemos los ids de todos los proyectos a los que pertenecen los stories
        const projectsId = stories.map(story => {
            const project: any = story.project;
            return project.id as number;
        });

        // Eliminamos los duplicados de la lista anterior
        const projectsIdSet = new Set(projectsId);

        // Construimos un objeto cuyos keys son los ids de los proyectos
        const storiesByProject: any = {};

        projectsIdSet.forEach(projectId => {
            storiesByProject[projectId] = stories.filter(story => {
                const project: any = story.project;
                return project.id === projectId;
            });
        });

        return storiesByProject;
    }

    const renderTaskCards = (tasks: UserStory[]) => {
        const groupedTasks: any = groupTasksByProject(tasks);
        const projectsId = Object.keys(groupedTasks);
        return projectsId.map(projectId => {

            const proyecto = proyectos.find(proyecto => proyecto.id === Number(projectId));

            if (!proyecto) {
                return;
            }

            if (proyecto.status === "FINALIZADO") {
                return;
            }

            return <Box key={projectId} style={{ marginBottom: 20 }}>
                <Typography style={{ fontSize: 20, fontWeight: 'bold' }}>{proyecto.name}</Typography>
                {
                    groupedTasks[Number(projectId)].map((task: any) => (
                        <Card key={task.id} id={task.id} style={{ marginBottom: 15, cursor: 'pointer' }}
                            onClick={() => {
                                setSavedUserStory(task as UserStory);
                                dispatch(setCurrentUS(task as UserStory));
                                setOpen(true);
                            }}
                        >
                            <CardHeader
                                title={
                                    <Typography style={{ fontSize: 20 }}>
                                        {task.name}
                                    </Typography>
                                }
                                action={
                                    <IconButton aria-label="settings">
                                        <MoreVertIcon />
                                    </IconButton>
                                }
                            />
                        </Card>

                    ))
                }
            </Box>
        })
    }

    return (
        <Box style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
        }}>
            {
                renderTaskCards(tasks)
            }
            <UserStoryCardComponent isOpen={open} handleClose={() => setOpen(false)} readOnly />
        </Box>
    )
}

const MyDesktopPage = () => {
    const classes = useStyles();
    const [tasks, setTasks] = useState<UserStory[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    //const { state: { auth: { tasks } } } = useContext(AuthContext);

    useEffect(() => {
        (async () => {
            setLoading(true);
            try {
                let userTasks = await authAPI.getCurrentUserTasks();
                setTasks(userTasks);
            } catch (error) {
                console.log(error);
            } finally {
                setLoading(false);
            }
        })();
    }, []);

    return (
        !loading ?
            <Box component="main" className={classes.content}>
                <Box className={classes.myProyects}>
                    <Box>
                        <Typography style={{ textAlign: 'left' }} variant="h2" >Mis Tareas</Typography>
                    </Box>
                    <br />
                    <Divider></Divider>
                    <br />
                    <div className={classes.showingProjectCards}>
                        {
                            tasks?.length ?
                                <WithTaskComponent tasks={tasks} /> :
                                <WithoutTaskComponent />
                        }

                    </div>
                </Box>
            </Box> :
            <LoaderComponent />
    );
}
export default MyDesktopPage;
import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Member } from '../shared/interfaces/miembro.interface';
import { MemberRaw } from '../shared/interfaces/miembro-raw.interface';

class MemberScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({});
    }



    async postMiembro(id_project: number, miembro: MemberRaw): Promise<Member> {
        this._setToken();
        let response = await this.api.post(`/projectModule/projects/${id_project}/members/`, {
            ...miembro
        })
        return response.data;
    }

    async putMiembro(id_project: number, id_member: number, miembro: MemberRaw): Promise<Member> {
        this._setToken();
        let response = await this.api.put(`/projectModule/projects/${id_project}/members/${id_member}/`, { ...miembro })
        return response.data;
    }

    async deleteMiembro(id_project: number, id_member: number): Promise<void> {
        this._setToken();
        let response = await this.api.delete(`/projectModule/projects/${id_project}/members/${id_member}/`)
        return response.data;
    }

    async getAllMiembros(id_project: number): Promise<Member[]> {
        this._setToken();
        let response = await this.api.get(`/projectModule/projects/${id_project}/members/`);
        return response.data;
    }

    _setToken() {
        const token = localStorage.getItem('token');
        this.api = axios.create({
            baseURL: serverURL,
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
    }
}

export default new MemberScrumpyAPI();
import axios, { AxiosInstance } from 'axios';
import { Permiso } from '../shared/interfaces/permiso.interface';
import { Role } from '../shared/interfaces/role.interface';
import { serverURL } from '../config';

class RolScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL,
        });
    }

    async getRoles(): Promise<Role[]>{
        const token = localStorage.getItem('token');
        let response = await this.api.get('/roles/', {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async postRol(rol: Role): Promise<Role>{
        const token = localStorage.getItem('token');
        let response = await this.api.post('/roles/', {...rol}, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async getPermisos(): Promise<Permiso[]>{
        const token = localStorage.getItem('token');
        let response = await this.api.get('/permissions/', {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async putRol(rol: Role): Promise<Role> {
        const token = localStorage.getItem('token');
        let response = await this.api.put(`/roles/${rol.id}/`, {...rol}, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }

    async deleteRol(rol: Role): Promise<Role> {
        const token = localStorage.getItem("token")
        let response = await this.api.delete(`/roles/${rol.id}/`, {
            headers: {
                'Authorization': `JWT ${token}`,
            },
        });
        return response.data;
    }
}

export default new RolScrumpyAPI();
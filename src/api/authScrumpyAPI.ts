import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { Usuario } from '../shared/interfaces/usuario.interface';
import { Auth } from '../shared/interfaces/auth.interface';
import { UserStory } from '../shared/interfaces/user_story.interface';

class ScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL,
        });
    }

    async login(token_id: string): Promise<Auth> {
        let response = await this.api.post('/login/', {
            id_token: token_id,
        });

        let datos = response.data as Auth;
        const { usuario, token } = datos;

        console.log('Te logueaste como: ' + JSON.stringify(usuario));

        return {
            usuario,
            tasks: [],
            loggedIn: true,
            token,
        };
    }

    async getProfile(token: string): Promise<Auth> {
        let response = await this.api.get('/profile/', {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });

        let responseTasks = await this.api.get('/tasks/', {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });

        console.log('********* TASKS ************');
        console.log(responseTasks.data);

        const usuario = response.data as Usuario;

        return {
            usuario,
            tasks: responseTasks.data,
            loggedIn: true,
            token,
        };
    }

    async getCurrentUserTasks(): Promise<UserStory[]> {
        const token = localStorage.getItem('token') as string;

        let response = await this.api.get('/tasks/', {
            headers: {
                'Authorization': `JWT ${token}`,
            }
        });

        return response.data; 
    }
}

export default new ScrumpyAPI();
import axios, { AxiosInstance } from 'axios';
import { serverURL } from '../config';
import { UserStory } from '../shared/interfaces/user_story.interface';
import { Usuario } from '../shared/interfaces/usuario.interface';

class UsuarioScrumpyAPI {
    private api: AxiosInstance;
    constructor() {
        this.api = axios.create({
            baseURL: serverURL,
        });
    }

    async getUsuarios(token: string): Promise<Usuario[]> {
        let response = await this.api.get('/users/', {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }

    async postUsuario(usuario: Usuario): Promise<Usuario> {
        const token = localStorage.getItem('token') as string;
        let user = {...usuario};
        delete user.picture;
        let response = await this.api.post('/users/', {...user}, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }

    async updateUsuario(usuario: Usuario): Promise<Usuario> {
        const token = localStorage.getItem('token') as string;
        let user = {...usuario};
        delete user.picture;
        let response = await this.api.put(`/users/${usuario.id}/`, {...user}, {
            headers: {
                'Authorization': `JWT ${token}`
            }
        });
        return response.data;
    }
}

export default new UsuarioScrumpyAPI();
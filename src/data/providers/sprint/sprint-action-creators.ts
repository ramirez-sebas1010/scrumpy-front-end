import { Dispatch } from "react"
import sprintScrumpyAPI from "../../../api/sprintScrumpyAPI"
import { Action } from "../../../shared/interfaces/action.interface"
import { Sprint } from "../../../shared/interfaces/sprint.interface"

import { SprintActionType } from "./sprint-action-types"
import { PayloadType } from "./sprint-context"

export const getSprints = (sprints: Sprint[]) => ({
    type: SprintActionType.GET_SPRINTS,
    payload: sprints,
})

export const setLoading = () => ({
    type: SprintActionType.LOADING,
    payload: true,
})

export const setLoaded = () => ({
    type: SprintActionType.LOADING,
    payload: false,
})

export const addSprint = (sprint: Sprint) => ({
    type: SprintActionType.ADD_SPRINT,
    payload: sprint,
})

export const updateSprint = (sprint: Sprint) => ({
    type: SprintActionType.UPDATE_SPRINT,
    payload: sprint,
})

export const setCurrentSprint = (sprint: Sprint) => ({
    type: SprintActionType.SET_CURRENT_SPRINT,
    payload: sprint,
})

export const setSelectedSprint = (selectedSprint: number) => ({
    type: SprintActionType.SET_SELECTED_SPRINT,
    payload: selectedSprint,
})

export const postSprint = async (dispatch: Dispatch<Action<PayloadType>>, sprint: Partial<Sprint>, id_proyecto: number) => {
    dispatch(setLoading());
    try {
        console.log("En el post");
        let sprintNuevo = await sprintScrumpyAPI.postSprint(id_proyecto, sprint);
        console.log("En el post mori");
        console.log(sprintNuevo);
        dispatch(addSprint(sprintNuevo));
        return sprintNuevo;
    } catch(error:any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}

export const putSprint = async (dispatch: Dispatch<Action<PayloadType>>, sprint: Sprint, id_proyecto: number, id_sprint: number) => {
    dispatch(setLoading());
    try {
        let sprintActualizado = await sprintScrumpyAPI.updateSprint(id_proyecto, sprint, id_sprint);
        dispatch(updateSprint(sprintActualizado));
    } catch(error:any) {
        console.log(error.message)
    } finally {
        dispatch(setLoaded())
    }
}
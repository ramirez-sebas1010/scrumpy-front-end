import { Dispatch } from "react"
import rolScrumpyAPI from "../../../api/rolScrumpyAPI"
import { Action } from "../../../shared/interfaces/action.interface"
import { Role } from "../../../shared/interfaces/role.interface"
import { Permiso } from "../../../shared/interfaces/permiso.interface"
import { RolActionType } from "./rol-actions-types"

export const getRoles = (roles: Role[]) =>{
    return {
        type: RolActionType.GET_ROLES,
        payload: roles,
    }

}

export const addRol = (rol: Role) =>{
    return {
        type: RolActionType.ADD_ROL,
        payload: rol,
    }
}

export const setLoading = () =>({
    type: RolActionType.LOADING,
    payload: true,
})

export const setLoaded = () =>({
    type: RolActionType.LOADING,
    payload: false,
})

export const updateRol = (rol: Role) => ({
    type: RolActionType.UPDATE_ROL,
    payload: rol,
});

export const deleteRolAction = (rol: Role) => ({
    type: RolActionType.DELETE_ROL,
    payload: rol,
});

export const postRol = async (dispatch: Dispatch<Action<Role|Role[]|boolean>>, rol: Role) => {
    dispatch(setLoading());
    try {
        let rolNuevo = await rolScrumpyAPI.postRol(rol);
        dispatch(addRol(rolNuevo));
    } catch(error:any) {
        console.log(error.message);
    } finally {
        dispatch(setLoaded())
    }
}

export const getPermisos = (permisos: Permiso[]) =>{
    return {
        type: RolActionType.GET_PERMISOS,
        payload: permisos,
    }

}

export const getPermisosAPI = async (dispatch: Dispatch<Action<Role|Role[]|boolean | Permiso[]>>) => {
    try {
        let permisos = await rolScrumpyAPI.getPermisos();
        console.log(JSON.stringify(permisos));
        dispatch(getPermisos(permisos));
    } catch(error:any) {
        console.log(error.message);
    }
}

export const putRol = async (dispatch: Dispatch<Action<Role|Role[]|boolean | Permiso[]>>, rol: Role) => {
    try {
        let updatedRol = await rolScrumpyAPI.putRol(rol);
        dispatch(updateRol(updatedRol));
    } catch(error) {

    }
}

export const deleteRol = async (dispatch: Dispatch<Action<Role|Role[]|boolean | Permiso[]>>, rol: Role) => {
    dispatch(setLoading());
    await rolScrumpyAPI.deleteRol(rol);
    dispatch(deleteRolAction(rol));
    dispatch(setLoaded())
}

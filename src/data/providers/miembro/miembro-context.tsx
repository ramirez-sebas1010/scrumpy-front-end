import { Dispatch, useEffect, createContext, useReducer, FC, useContext } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';
import { Member, MemberInitializer } from "../../../shared/interfaces/miembro.interface";
import { getAllMiembros, setCurrentMember, setLoaded, setLoading } from './miembro-action-creators';
import { MeimbroActionType } from './miembro-action-types';
import equipoAPI from '../../../api/memberScrumpyAPI';
import { AuthContext } from '../auth/auth-context';
import { ProyectoContext } from '../proyecto/proyecto-context';
import { Usuario } from '../../../shared/interfaces/usuario.interface';

type MiembroState = {
    miembros: Member[];
    currentMember: Member;
    loading: boolean;
};

export type replace = {
    seleccionadoUserReemplazado: Usuario,
    seleccionadoMiembroReemplazado: Member
}

export type PayloadType = Member[] | Member | boolean | replace;

type MiembroContextType = {
    state: MiembroState;
    dispatch: Dispatch<Action<PayloadType>>;
};

const initialState: MiembroState = {
    miembros: [],
    currentMember: MemberInitializer,
    loading: false,
};

const initialContext: MiembroContextType = {
    state: initialState,
    dispatch: () => { },
}

export const MiembroContext = createContext<MiembroContextType>(initialContext);

const reducer = (state: MiembroState, action: Action<PayloadType>): MiembroState => {
    const { miembros: currentMiembros } = state;
    switch (action.type) {
        case MeimbroActionType.GET_ALL_MIEMBROS:
            return {
                ...state,
                miembros: action.payload as Member[]
            }
        case MeimbroActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case MeimbroActionType.ADD_MIEMBRO:
            return {
                ...state,
                miembros: [...currentMiembros, { ...action.payload as Member }]
            }
        case MeimbroActionType.UPDATE_MIEMBRO:
            let actualizacion: Member = { ...action.payload as Member };
            let miembroActualizados = currentMiembros.map(miembro => {
                if (miembro.id === actualizacion.id) {
                    return {
                        ...miembro,
                        ...actualizacion,
                    }
                }
                return { ...miembro };
            });
            return {
                ...state,
                miembros: [...miembroActualizados]
            }
        case MeimbroActionType.DELETE_MIEMBRO:
            let miembro_eliminar: Member = { ...action.payload as Member };
            let miembroActualizadosLuegoDeEliminar = currentMiembros.filter(miembro => miembro.id !== miembro_eliminar.id);
            return {
                ...state,
                miembros: [...miembroActualizadosLuegoDeEliminar]
            }
        case MeimbroActionType.SET_CURRENT_MEMBER:
            return {
                ...state,
                currentMember: { ...action.payload as Member }
            }
        case MeimbroActionType.SWITCH_MIEMBRO:
            const { seleccionadoUserReemplazado } = action.payload as replace;
            const { seleccionadoMiembroReemplazado } = action.payload as replace;
            let miembrosActualizado: Member[] = currentMiembros.map(m => {
                if (m.id === seleccionadoMiembroReemplazado.id) {
                    m.user = seleccionadoUserReemplazado;
                }
                return { ...m };
            });

            return {
                ...state,
                miembros: [...miembrosActualizado]
            }
        default:
            return {
                ...state
            }
    }
}

export const MiembroProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const { state: { auth: { loggedIn, usuario } } } = useContext(AuthContext);

    const { state: { currentProject } } = useContext(ProyectoContext);

    useEffect(() => {
        (async () => {
            try {
                dispatch(setLoading());
                let miembros = await equipoAPI.getAllMiembros(currentProject.id as number);
                dispatch(getAllMiembros(miembros));
                const currentMember = miembros.find(miembro => miembro.user.id === usuario.id);
                currentMember && dispatch(setCurrentMember(currentMember));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [loggedIn, currentProject]);

    return (
        <MiembroContext.Provider value={{ state, dispatch }}>
            {children}
        </MiembroContext.Provider>
    );
}

import { Dispatch, useEffect, createContext, useReducer, FC, useContext } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';
import { setLoading, setLoaded, getAllEvents } from "./eventUS-action-creators";
import { ProyectoContext } from '../proyecto/proyecto-context';
import { UserStoryContext } from '../userstory/user-story-context'
import { EventUS } from '../../../shared/interfaces/eventUS.interface';
import { EventUSActionType } from './eventUS-action-types';
import eventUSScrumpyAPI from '../../../api/eventUSScrumpyAPI';

type EventUSState = {
    events: EventUS[],
    loading: boolean;
}

export type PayloadType = EventUS[] | EventUS | boolean;

type EventUSContextType = {
    state: EventUSState;
    dispatch: Dispatch<Action<PayloadType>>;
}

const initialState: EventUSState = {
    events: [],
    loading: false,
}

const initialContext: EventUSContextType = {
    state: initialState,
    dispatch: () => { },
}

export const EventUSContext = createContext<EventUSContextType>(initialContext);

const reducer = (state: EventUSState, action: Action<PayloadType>): EventUSState => {
    const { events: currentEventUSs } = state;
    switch (action.type) {
        case EventUSActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case EventUSActionType.GET_EVENTS:
            return {
                ...state,
                events: action.payload as EventUS[]
            }
        default:
            return {
                ...state
            }
    }
}

export const EventUSProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);

    useEffect(() => {
        (async () => {
            try {
                dispatch(setLoading());
                let events = await eventUSScrumpyAPI.getAllEvents(currentProject.id as number, currentUS.id) as EventUS[];
                dispatch(getAllEvents(events));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [currentProject, currentUS]);

    return (
        <EventUSContext.Provider value={{ state, dispatch }}>
            {children}
        </EventUSContext.Provider>
    );
}
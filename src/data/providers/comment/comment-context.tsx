import { Dispatch, useEffect, createContext, useReducer, FC, useContext } from 'react';
import { Action } from '../../../shared/interfaces/action.interface';
import { CommentActionType } from "./comment-action-types";
import { Comment } from "../../../shared/interfaces/comment.interface"
import CommentScrumpyAPI from "../../../api/commentScrumpyAPI";
import { setLoading, setLoaded, getAllComments } from "./comment-action-creators";
import { ProyectoContext } from '../proyecto/proyecto-context';
import { UserStoryContext } from '../userstory/user-story-context'

type CommentState = {
    comments: Comment[],
    loading: boolean;
}

export type PayloadType = Comment[] | Comment | boolean;

type CommentContextType = {
    state: CommentState;
    dispatch: Dispatch<Action<PayloadType>>;
}

const initialState: CommentState = {
    comments: [],
    loading: false,
}

const initialContext: CommentContextType = {
    state: initialState,
    dispatch: () => { },
}

export const CommentContext = createContext<CommentContextType>(initialContext);

const reducer = (state: CommentState, action: Action<PayloadType>): CommentState => {
    const { comments: currentComments } = state;
    switch (action.type) {
        case CommentActionType.LOADING:
            return {
                ...state,
                loading: action.payload as boolean,
            }
        case CommentActionType.GET_COMMENTS:
            return {
                ...state,
                comments: action.payload as Comment[]
            }
        case CommentActionType.ADD_COMMENT:
            return {
                ...state,
                comments: [...currentComments, { ...action.payload as Comment }]
            }
        case CommentActionType.UPDATE_COMMENT:
            let actualizacion: Comment = { ...action.payload as Comment };
            let commentsActualizados = currentComments.map(comment => {
                if (comment.id === actualizacion.id) {
                    return {
                        ...comment,
                        ...actualizacion,
                    }
                }
                return { ...comment };
            });
            return {
                ...state,
                comments: [...commentsActualizados]
            }
        case CommentActionType.DELETE_COMMENT:
            const commentToDelete = action.payload as Comment;
            return {
                ...state,
                comments: state.comments.filter(comment => comment.id != commentToDelete.id),
            }
        default:
            return {
                ...state
            }
    }
}

export const CommentProvider: FC = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { state: { currentProject } } = useContext(ProyectoContext);
    const { state: { currentUS } } = useContext(UserStoryContext);

    useEffect(() => {
        (async () => {
            try {
                dispatch(setLoading());
                let comentarios = await CommentScrumpyAPI.getAllComments(currentProject.id as number, currentUS.id) as Comment[];
                dispatch(getAllComments(comentarios));
            } catch (error) {
                console.log(error);
            } finally {
                dispatch(setLoaded());
            }
        })();
    }, [currentProject, currentUS]);

    return (
        <CommentContext.Provider value={{ state, dispatch }}>
            {children}
        </CommentContext.Provider>
    );
}